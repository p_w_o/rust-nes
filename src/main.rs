use std::fs::{self, File};
use std::path::PathBuf;
use std::thread::sleep;
use std::time::{Duration, Instant};

use clap::{Parser, ValueEnum};

use nes::debug::Debug;
use nes::emulator::Emulator;
use nes::ines::Rom;

use sdl2::{filesystem, Sdl};
use ui::audio::Audio;
use ui::graphics::Graphics;
use ui::input::Input;
use ui::debugger::{DebugUi, CpuDump};

use sdl::sdl_audio::SdlAudio;

use crate::nes::emulator::{EmulatorAction, self};
use crate::rodio_audio::RodioAudio;
use crate::sdl::sdl_debugger::{SdlBgDebugger, SdlSpriteDebugger};
use crate::sdl::sdl_graphics::SdlGraphics;
use crate::sdl::sdl_input::SdlInput;
use crate::ui::input::Action;

mod nes;
mod sdl;
mod ui;
mod util;
mod rodio_audio;


#[derive(ValueEnum, Copy, Clone, Debug)]
enum AudioImplementation {
	/// SDL2 based audio implementation
	Sdl,
	/// Rodio based audio implementation with dynamic FPS support
	Rodio,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Opt {

	/// The audio implementation to use
	#[arg(short, long)]
	audio: Option<AudioImplementation>,

	/// Dump CPU state before each instruction
	#[arg(short, long)]
	dump: bool,

	/// Enable nametable debugger
	#[arg(short, long)]
	bg: bool,

	/// Enable sprite debugger
	#[arg(short, long)]
	sprites: bool,

	/// ROM file in ines format
	rom: PathBuf,

}


fn main() {
	let opt = Opt::parse();

	let rom = read_rom(&opt.rom);
	println!("ROM: {:#?}", rom.header);

	let sdl_context = sdl2::init().unwrap();
	let mut input = SdlInput::new(sdl_context.clone());
	let mut graphics = SdlGraphics::new(sdl_context.clone());
	let mut audio: Box<dyn Audio> = match opt.audio.unwrap_or(AudioImplementation::Rodio) {
		AudioImplementation::Sdl => Box::new(SdlAudio::new(sdl_context.clone())),
		AudioImplementation::Rodio => Box::new(RodioAudio::new()),
	};

	let crc32 = rom.crc32;
	let fps_default = 60.0;
	let mut fps_limiter = FpsLimiter::new(fps_default);

	let (debuggers, mut debugger_uis) = init_debuggers(sdl_context, opt);
	let (rx_emulator_out, tx_emulator_in) = emulator::new_emulator_thread(rom, debuggers);

	let mut frame = 0;
	let mut save_slot: u32 = 0;
	let mut fps_show = false;

	tx_emulator_in.send(EmulatorAction::EmulateFrame { pad0: 0, pad1: 0 }).unwrap();
	loop {
		frame += 1;
		let emulator_output = rx_emulator_out.recv().unwrap();

		let state = input.handle_input();
		for action in state.actions {
			match action {
				Action::Quit => return,
				Action::SaveSlotPrev => {
					save_slot = (save_slot - 1 + 5) % 5;
					show_save_slot(save_slot, crc32, &mut graphics);
				},
				Action::SaveSlotNext => {
					save_slot = (save_slot + 1) % 5;
					show_save_slot(save_slot, crc32, &mut graphics);
				}
				Action::Fullscreen => graphics.set_fullscreen(true),
				Action::Windowed => graphics.set_fullscreen(false),
				Action::Load => tx_emulator_in.send(EmulatorAction::Load(get_save_file(crc32, save_slot))).expect("Failed sending Load action to emulator"),
				Action::Save => tx_emulator_in.send(EmulatorAction::Save(get_save_file(crc32, save_slot), emulator_output.picture.to_vec())).expect("Failed sending Save action to emulator"),
				Action::FpsReset => fps_limiter.set(fps_default),
				Action::FpsIncrease => fps_limiter.increment(5.0),
				Action::FpsDecrease => fps_limiter.increment(-5.0),
				Action::FpsLimitToggle => fps_limiter.toggle(),
				Action::FpsShowToggle => {
					fps_show = !fps_show;
					graphics.set_show_fps(fps_show);
				},
			}
		}

		fps_limiter.limit();
		tx_emulator_in.send(EmulatorAction::EmulateFrame { pad0: state.pads[0], pad1: state.pads[1] }).unwrap();

		if ((frame + 1) % 30) == 0 {
			let fps = fps_limiter.fps();
			graphics.set_fps(fps);
			audio.set_fps(fps);
		}

		graphics.draw(&emulator_output.picture);
		audio.play(&emulator_output.samples);

		for dbg in debugger_uis.iter_mut() {
			dbg.present();
		}
	}
}

fn show_save_slot(save_slot: u32, crc32: u32, graphics: &mut SdlGraphics) {
	let picture = Emulator::load_image(&get_save_file(crc32, save_slot));
	graphics.show_save_slot(save_slot + 1, picture);
}


fn get_save_file(crc32: u32, save_slot: u32) -> PathBuf {
	let save_dir = filesystem::pref_path("rust-nes", "saves").unwrap();
	fs::create_dir_all(&save_dir).unwrap();
    PathBuf::from(format!("{}/{:08x}_{}.sav", save_dir, crc32, save_slot))
}


fn read_rom(file_name: &PathBuf) -> Rom {
	let mut file = File::open(file_name).unwrap();
	Rom::read(&mut file).unwrap()
}


fn init_debuggers(sdl_context: Sdl, opt: Opt) -> (Vec<Box<dyn Debug>>, Vec<Box<dyn DebugUi>>) {
	let mut debuggers: Vec<Box<dyn Debug>> = Vec::new();
	let mut debugger_uis: Vec<Box<dyn DebugUi>> = Vec::new();
	if opt.dump {
		debuggers.push(Box::new(CpuDump::new()));
	}

	if opt.bg {
		let mut dbg = SdlBgDebugger::new(sdl_context.clone());
		debuggers.push(dbg.debugger());
		debugger_uis.push(Box::new(dbg));
	}

	if opt.sprites {
		let mut dbg = SdlSpriteDebugger::new(sdl_context);
		debuggers.push(dbg.debugger());
		debugger_uis.push(Box::new(dbg));
	}

	(debuggers, debugger_uis)
}


struct FpsLimiter {
	frame_start: Instant,
	target_fps: f64,
	frame_duration: Duration,
	frame_durations: Vec<Duration>,
}


impl FpsLimiter {
	fn new(target_fps: f64) -> FpsLimiter {
		let mut result = FpsLimiter {
			frame_start: Instant::now(),
			target_fps,
			frame_duration: Duration::ZERO,
			frame_durations: Vec::new(),
		};
		result.set(target_fps);
		result
	}

	fn set(&mut self, target_fps: f64) {
		self.target_fps = target_fps;
		self.frame_duration = Duration::new(0, (1_000_000_000f64 / target_fps) as u32);
	}

	fn toggle(&mut self) {
		if self.frame_duration == Duration::ZERO {
			self.set(self.target_fps);
		}
		else {
			self.frame_duration = Duration::ZERO;
		}
	}

	fn increment(&mut self, amount: f64) {
		let mut target = self.target_fps + amount;
		if target < 1.0 {
			target = 1.0;
		}
		else if target > 200.0 {
			target = 200.0;
		}
		self.set(target);
	}

	fn limit(&mut self) {
		let start = self.frame_start;
		let elapsed = start.elapsed();
		if elapsed < self.frame_duration {
			sleep(self.frame_duration - elapsed);
		}
		self.frame_start = Instant::now();
		self.frame_durations.push(self.frame_start.duration_since(start));
	}

	fn fps(&mut self) -> f64 {
		let mut total = 0.0;
		for duration in &self.frame_durations {
			total += duration.as_secs_f64();
		}
		let result = self.frame_durations.len() as f64 / total;
		self.frame_durations.clear();
		result
	}

}
