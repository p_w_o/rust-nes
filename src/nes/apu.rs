use super::mapper::Mapper;
use super::filter::{Filter, HighPassFilter, LowPassFilter};

use std::cell::RefCell;
use std::rc::Rc;

const PULSE_LOOKUP: [f32; 31] = [
	0.000000, 0.011609, 0.022939, 0.034001, 0.044803, 0.055355, 0.065665, 0.075741,
	0.085591, 0.095224, 0.104645, 0.113862, 0.122882, 0.131710, 0.140353, 0.148816,
	0.157105, 0.165226, 0.173183, 0.180981, 0.188626, 0.196120, 0.203470, 0.210679,
	0.217751, 0.224689, 0.231499, 0.238182, 0.244744, 0.251186, 0.257513,
];

const TND_LOOKUP: [f32; 203] = [
	0.000000, 0.006700, 0.013345, 0.019936, 0.026474, 0.032959, 0.039393, 0.045775,
	0.052106, 0.058386, 0.064618, 0.070800, 0.076934, 0.083020, 0.089058, 0.095050,
	0.100996, 0.106896, 0.112751, 0.118561, 0.124327, 0.130049, 0.135728, 0.141365,
	0.146959, 0.152512, 0.158024, 0.163494, 0.168925, 0.174315, 0.179666, 0.184978,
	0.190252, 0.195487, 0.200684, 0.205845, 0.210968, 0.216054, 0.221105, 0.226120,
	0.231099, 0.236043, 0.240953, 0.245828, 0.250669, 0.255477, 0.260252, 0.264993,
	0.269702, 0.274379, 0.279024, 0.283638, 0.288220, 0.292771, 0.297292, 0.301782,
	0.306242, 0.310673, 0.315074, 0.319446, 0.323789, 0.328104, 0.332390, 0.336649,
	0.340879, 0.345083, 0.349259, 0.353408, 0.357530, 0.361626, 0.365696, 0.369740,
	0.373759, 0.377752, 0.381720, 0.385662, 0.389581, 0.393474, 0.397344, 0.401189,
	0.405011, 0.408809, 0.412584, 0.416335, 0.420064, 0.423770, 0.427454, 0.431115,
	0.434754, 0.438371, 0.441966, 0.445540, 0.449093, 0.452625, 0.456135, 0.459625,
	0.463094, 0.466543, 0.469972, 0.473380, 0.476769, 0.480138, 0.483488, 0.486818,
	0.490129, 0.493421, 0.496694, 0.499948, 0.503184, 0.506402, 0.509601, 0.512782,
	0.515946, 0.519091, 0.522219, 0.525330, 0.528423, 0.531499, 0.534558, 0.537601,
	0.540626, 0.543635, 0.546627, 0.549603, 0.552563, 0.555507, 0.558434, 0.561346,
	0.564242, 0.567123, 0.569988, 0.572838, 0.575673, 0.578493, 0.581298, 0.584088,
	0.586863, 0.589623, 0.592370, 0.595101, 0.597819, 0.600522, 0.603212, 0.605887,
	0.608549, 0.611197, 0.613831, 0.616452, 0.619059, 0.621653, 0.624234, 0.626802,
	0.629357, 0.631899, 0.634428, 0.636944, 0.639448, 0.641939, 0.644418, 0.646885,
	0.649339, 0.651781, 0.654212, 0.656630, 0.659036, 0.661431, 0.663813, 0.666185,
	0.668544, 0.670893, 0.673229, 0.675555, 0.677869, 0.680173, 0.682465, 0.684746,
	0.687017, 0.689276, 0.691525, 0.693763, 0.695991, 0.698208, 0.700415, 0.702611,
	0.704797, 0.706973, 0.709139, 0.711294, 0.713440, 0.715576, 0.717702, 0.719818,
	0.721924, 0.724021, 0.726108, 0.728186, 0.730254, 0.732313, 0.734362, 0.736402,
	0.738433, 0.740455, 0.742468,
];

const LENGTHS: [u8; 32] = [
		10, 254, 20, 2,  40, 4,  80, 6,  160, 8,  60, 10, 14, 12, 26, 14,
		12, 16,  24, 18, 48, 20, 96, 22, 192, 24, 72, 26, 16, 28, 32, 30
];


const PULSE_DUTIES: [[bool; 8]; 4] = [
		[ false, true,  false, false, false, false, false, false ],
		[ false, true,  true,  false, false, false, false, false ],
		[ false, true,  true,  true,  true,  false, false, false ],
		[ true,  false, false, true,  true,  true,  true,  true  ],
];

const NOISE_PERIODS: [u16; 16] = [ 4, 8, 16, 32, 64, 96, 128, 160, 202, 254, 380, 508, 762, 1016, 2034, 4068 ];

const DMC_PERIODS: [u16; 16] = [ 214, 190, 170, 160, 143, 127, 113, 107, 95, 80, 71, 64, 53, 42, 36, 27 ];


const REG_PULSE_1_CONTROL: u16  = 0x4000;   // DDLC NNNN 	Duty, loop envelope/disable length counter, constant volume, envelope period/volume
const REG_PULSE_1_SWEEP: u16    = 0x4001;   // EPPP NSSS 	Sweep unit: enabled, period, negative, shift count
const REG_PULSE_1_TIMER: u16    = 0x4002;   // LLLL LLLL 	Timer low
const REG_PULSE_1_LENGTH: u16   = 0x4003;   // LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)

const REG_PULSE_2_CONTROL: u16  = 0x4004;   // DDLC NNNN 	Duty, loop envelope/disable length counter, constant volume, envelope period/volume
const REG_PULSE_2_SWEEP: u16    = 0x4005;   // EPPP NSSS 	Sweep unit: enabled, period, negative, shift count
const REG_PULSE_2_TIMER: u16    = 0x4006;   // LLLL LLLL 	Timer low
const REG_PULSE_2_LENGTH: u16   = 0x4007;   // LLLL LHHH 	Length counter load, timer high (also resets duty and starts envelope)

const REG_TRIANGLE_LINEAR: u16  = 0x4008;   // CRRR RRRR 	Length counter disable/linear counter control, linear counter reload value
const REG_TRIANGLE_TIMER: u16   = 0x400A;   // LLLL LLLL 	Timer low
const REG_TRIANGLE_LENGTH: u16  = 0x400B;   // LLLL LHHH 	Length counter load, timer high (also reloads linear counter)

const REG_NOISE_ENVELOPE: u16   = 0x400C;   // --LC NNNN 	Loop envelope/disable length counter, constant volume, envelope period/volume
const REG_NOISE_PERIOD: u16     = 0x400E;   // L--- PPPP 	Loop noise, noise period
const REG_NOISE_LENGTH: u16	    = 0x400F;   // LLLL L--- 	Length counter load (also starts envelope)

const REG_DMC_CONTROL: u16      = 0x4010;   // IL-- FFFF 	IRQ enable, loop sample, frequency index
const REG_DMC_LOAD: u16         = 0x4011;   // -DDD DDDD 	Direct load
const REG_DMC_ADDRESS: u16      = 0x4012;   // AAAA AAAA 	Sample address %11AAAAAA.AA000000
const REG_DMC_LENGTH: u16	    = 0x4013;   // LLLL LLLL 	Sample length %0000LLLL.LLLL0001

const REG_STATUS: u16           = 0x4015;   // ---D NT21 	Control: DMC enable, length counter enables: noise, triangle, pulse 2, pulse 1 (write)
                                            // IF-D NT21 	Status: DMC interrupt, frame interrupt, length counter status: noise, triangle, pulse 2, pulse 1 (read)
const REG_FRAME_COUNTER: u16    = 0x4017;   // SD-- ---- 	Frame counter: 5-frame sequence, disable frame interrupt (write)


#[derive(Debug)]
struct Envelope {
	period: u8,
	looping: bool,
	enabled: bool,
	reset: bool,

	decay: u8,
	step: u8,
}

#[derive(Debug)]
struct Sweep {
	enabled: bool,
	period: u8,
	negate: bool,
	shift: u8,

	step: u8,
	reset: bool,
	mute: bool,
	ones_complement: bool,
}

#[derive(Debug)]
struct Pulse {
	envelope: Envelope,
	sweep: Sweep,
	duty: u8,
	length_counter: u8,
	timer_period: u16,  	// 11-bit timer for sequencer steps

	timer_value: u16,
	sequencer: u8,      	// 8-step sequencer over selected duty sequence
}


#[derive(Debug)]
struct LinearCounter {
	period: u8,
	control: bool,
	reset: bool,

	step: u8,
}


#[derive(Debug)]
struct Triangle {
	linear_counter: LinearCounter,
	length_counter: u8,

	timer_period: u16,  	// 11-bit timer for sequencer steps
	timer_value: u16,

	sequencer: u8,      	// 32-step sequencer: 15, 14, ..., 0, 0, 1, ..., 15
}


#[derive(Debug)]
struct Noise {
	envelope: Envelope,
	length_counter: u8,
	mode: bool,

	timer_period: u16,  	// 11-bit timer for sequencer steps
	timer_value: u16,

	noise_data: u16,
}


struct Dmc {
	mapper: Rc<RefCell<dyn Mapper>>,
	irq_enabled: bool,
	looping: bool,
	value: u8,

	sample_address: u16,
	sample_length: u16,
	bytes_left: u16,
	address: u16,
	buffer: u16,

	timer_period: u16,  	// 11-bit timer for sequencer steps
	timer_value: u16,
	interrupted: bool,
}


#[derive(Copy, Clone, Debug)]
enum FrameCounterMode {
	FourStepMode = 29830,
	FiveStepMode = 37282,
}


struct FrameCounter {
	irq_enabled: bool,
	mode: FrameCounterMode,
	cycle: u16,
	reset_counter: u8,
	interrupted: bool,
}


pub struct Apu {
	pulse1: Pulse,
	pulse2: Pulse,
	triangle: Triangle,
	noise: Noise,
	dmc: Dmc,
	frame_counter: FrameCounter,

	filters: Vec<Box<dyn Filter>>,
	pub samples: [f32; 29467],
	pub sample_count: u16,
}


impl Envelope {
	fn clock(&mut self) {
		if self.reset {
			self.decay = 15;
			self.step = self.period;
			self.reset = false;
		}
		else if self.step > 0 {
			self.step -= 1;
		}
		else {
			self.step = self.period;
			if self.decay > 0 {
				self.decay -= 1;
			}
			else if self.looping {
				self.decay = 15;
			}
		}
	}
	
	fn output(&self) -> u8 {
		if self.enabled {
			self.decay
		}
		else {
			self.period
		}
	}
	
	fn reg_write(&mut self, value: u8) {
		self.looping = value & 0x20 != 0;
		self.enabled = value & 0x10 == 0;
		self.period = value & 0x0F;
		self.reset = true;
	}
	
	
	fn new() -> Envelope {
		Envelope {
			period: 0,
			looping: false,
			enabled: false,
			reset: false,
			decay: 0,
			step: 0,
		}
	}
}


impl Sweep {
	fn new(ones_complement: bool) -> Sweep {
		Sweep {
			enabled: false,
			period: 0,
			negate: false,
			shift: 0,
			step: 0,
			reset: false,
			mute: false,
			ones_complement,
		}
	}
}


impl Pulse {
	fn clock_timer(&mut self) {
		if self.timer_value > 0 {
			self.timer_value -= 1;
		}
		else {
			self.timer_value = self.timer_period;
			self.sequencer = (self.sequencer + 1) % 8;
		}
	}
	
	fn clock_length_counter(&mut self) {
		if self.length_counter > 0 && !self.envelope.looping {
			self.length_counter -= 1;
		}
	}
	
	fn clock_sweep(&mut self) {
		if self.sweep.reset {
			self.sweep.reset = false;
			self.sweep.step = self.sweep.period + 1;
		}
	
		if self.sweep.step > 0 {
			self.sweep.step -= 1;
		}
		else {
			self.sweep.step = self.sweep.period + 1;
			if self.sweep.enabled {
				let delta = self.timer_period >> self.sweep.shift;
				if self.sweep.negate {
					self.timer_period -= delta;
					if self.sweep.ones_complement && self.timer_period > 0 {
						self.timer_period -= 1;
					}
				}
				else {
					let temp = self.timer_period + delta;
					self.sweep.mute = temp > 0x800;
					
					if !self.sweep.mute {
						self.timer_period = temp;
					}
				}
			}
		}
	}
	
	fn output(&self) -> u8 {
		if self.length_counter == 0 {
			return 0;
		}
		if self.timer_period < 8 {
			return 0;
		}
		if !PULSE_DUTIES[self.duty as usize][self.sequencer as usize] {
			return 0;
		}

		self.envelope.output()
	}

	fn reg_control_write(&mut self, value: u8) {
		self.duty = value >> 6;
		self.envelope.reg_write(value);
	}

	fn reg_sweep_write(&mut self, value: u8) {
		self.sweep.enabled = value & 0x80 != 0;
		self.sweep.period = (value >> 4) & 0x07;
		self.sweep.negate = value & 0x08 != 0;
		self.sweep.shift = value & 0x07;
		self.sweep.reset = true;
	}

	fn reg_timer_write(&mut self, value: u8) {
		self.timer_period = (self.timer_period & 0x0700) | value as u16;
	}

	fn reg_length_write(&mut self, value: u8) {
		self.timer_period = (self.timer_period & 0x00FF) | (((value as u16) << 8) & 0x0700);
		self.length_counter = LENGTHS[(value >> 3) as usize];
		self.envelope.reset = true;
		self.sequencer = 0;
	}


	fn new(ones_complement: bool) -> Pulse {
		Pulse {
			envelope: Envelope::new(),
			sweep: Sweep::new(ones_complement),
			duty: 0,
			length_counter: 0,
			timer_period: 0,
			timer_value: 0,
			sequencer: 0,
		}
	}
}


impl LinearCounter {
	fn clock(&mut self) {
		if self.reset {
			self.step = self.period;
			if !self.control {
				self.reset = false;
			}
		}
		else if self.step > 0 {
			self.step -= 1;
		}
	}
	
	fn new() -> LinearCounter {
		LinearCounter {
			period: 0,
			control: false,
			reset: false,
			step: 0,
		}
	}
}

impl Triangle {
	fn clock_timer(&mut self) {
		if self.timer_value > 0 {
			self.timer_value -= 1;
		}
		else {
			self.timer_value = self.timer_period;
			if self.length_counter > 0 && self.linear_counter.step > 0 {
				self.sequencer = (self.sequencer + 1) % 32;
			}
		}
	}
	
	fn clock_length_counter(&mut self) {
		if self.length_counter > 0 && !self.linear_counter.control {
			self.length_counter -= 1;
		}
	}
	
	fn output(&self) -> u8 {
		if self.length_counter == 0 {
			return 0;
		}
		if self.linear_counter.step == 0 {
			return 0;
		}
		
		let sequence = self.sequencer;
		if sequence <= 15 {
			15 - sequence
		}
		else {
			sequence - 16
		}
	}

	fn reg_linear_write(&mut self, value: u8) {
		self.linear_counter.control = value & 0x80 != 0;
		self.linear_counter.period = value & 0x7F;
	}

	fn reg_timer_write(&mut self, value: u8) {
		self.timer_period = (self.timer_period & 0x0700) | value as u16;
	}

	fn reg_length_write(&mut self, value: u8) {
		self.timer_period = (self.timer_period & 0x00FF) | (((value as u16) << 8) & 0x0700);
		self.length_counter = LENGTHS[(value >> 3) as usize];
		self.linear_counter.reset = true;
		self.sequencer = 0;
	}

	fn new() -> Triangle {
		Triangle {
			linear_counter: LinearCounter::new(),
			length_counter: 0,
			timer_period: 0,
			timer_value: 0,
			sequencer: 0,
		}
	}
	
}

impl Noise {
	fn clock_timer(&mut self) {
		if self.timer_value > 0 {
			self.timer_value -= 1;
		}
		else {
			self.timer_value = self.timer_period;
			let new_bit = (self.noise_data & 0x01) ^ ((self.noise_data & if self.mode { 0x40 } else { 0x02 }) != 0) as u16;
			self.noise_data = self.noise_data >> 1 | new_bit << 14;
		}
	}
	
	fn clock_length_counter(&mut self) {
		if self.length_counter > 0 && !self.envelope.looping {
			self.length_counter -= 1;
		}
	}
	
	fn output(&self) -> u8 {
		if self.length_counter == 0 {
			return 0;
		}
		if self.timer_period < 8 {
			return 0;
		}
		if self.noise_data & 1 == 1 {
			return 0;
		}

		self.envelope.output()
	}

	fn reg_period_write(&mut self, value: u8) {
		self.mode = value & 0x80 != 0;
		self.timer_period = NOISE_PERIODS[(value & 0x0F) as usize];
	}

	fn reg_length_write(&mut self, value: u8) {
		self.length_counter = LENGTHS[(value >> 3) as usize];
		self.envelope.reset = true;
	}

	fn new() -> Noise {
		Noise {
			envelope: Envelope::new(),
			length_counter: 0,
			mode: false,
			timer_period: 0,
			timer_value: 0,
			noise_data: 1,
		}
	}
	
}

impl Dmc {
	fn clock_timer(&mut self) {
		if self.timer_value > 0 {
			self.timer_value -= 1;
		}
		else {
			self.timer_value = self.timer_period;
			if self.buffer == 1 {
				if self.bytes_left == 0 {
					return;
				}
	
				self.buffer = 0x0400 | self.mapper.borrow_mut().cpu_read_ubyte(self.address) as u16;
				if self.address == 0xFFFF {
					self.address = 0x8000;
				}
				else {
					self.address += 1;
				}
				
				self.bytes_left -= 1;
				if self.bytes_left == 0 {
					if self.looping {
						self.address = self.sample_address;
						self.bytes_left = self.sample_length;
					}
					else if self.irq_enabled {
						self.interrupted = true;
					}
				}
			}
	
			if self.buffer & 1 != 0 {
				if self.value <= 125 {
					self.value += 2;
				}
			}
			else if self.value >= 2 {
				self.value -= 2;
			}
	
			self.buffer >>= 1;
		}
	}
	
	fn output(&self) -> u8 {
		self.value
	}
	
	fn reg_control_write(&mut self, value: u8) {
		self.irq_enabled = value & 0x80 != 0;
		if !self.irq_enabled {
			self.interrupted = false;
		}

		self.looping = value & 0x40 != 0;
		self.timer_period = DMC_PERIODS[(value & 0x0F) as usize];
	}

	fn reg_load_write(&mut self, value: u8) {
		self.value = value & 0x7F;
	}

	fn reg_address_write(&mut self, value: u8) {
		self.sample_address = 0xC000 + ((value as u16) << 6);
	}

	fn reg_length_write(&mut self, value: u8) {
		self.sample_length = ((value as u16) << 4) | 1;
	}

	fn new(mapper: Rc<RefCell<dyn Mapper>>) -> Dmc {
		Dmc {
			mapper,
			irq_enabled: false,
			looping: false,
			value: 0,
			sample_address: 0,
			sample_length: 0,
			bytes_left: 0,
			address: 0,
			buffer: 1,
			timer_period: 0,
			timer_value: 0,
			interrupted: false,
		}
	}
}


impl FrameCounter {
	fn new() -> FrameCounter {
		FrameCounter {
			irq_enabled: false,
			mode: FrameCounterMode::FourStepMode,
			cycle: 0,
			reset_counter: 0,
			interrupted: false,
		}
	}
}


impl Apu {
	fn clock_length_counters(&mut self) {
		self.pulse1.clock_length_counter();
		self.pulse2.clock_length_counter();
		self.triangle.clock_length_counter();
		self.noise.clock_length_counter();
	}
	
	fn clock_timers(&mut self) {
		if self.frame_counter.cycle & 1 == 0 {
			self.pulse1.clock_timer();
			self.pulse2.clock_timer();
			self.noise.clock_timer();
			self.dmc.clock_timer();
		}
		self.triangle.clock_timer();
	}
	
	
	fn clock_envelopes(&mut self) {
		self.pulse1.envelope.clock();
		self.pulse2.envelope.clock();
		self.noise.envelope.clock();
		self.triangle.linear_counter.clock();
	}
	
	fn clock_all(&mut self) {
		self.clock_length_counters();
		self.pulse1.clock_sweep();
		self.pulse2.clock_sweep();
	}
	
	pub fn emulate(&mut self) -> bool{
		if self.frame_counter.reset_counter == 1 {
			self.frame_counter.reset_counter = 0;
			self.frame_counter.cycle = 0;
		}
		else {
			if self.frame_counter.reset_counter > 0 {
				self.frame_counter.reset_counter -= 1;
			}
			self.frame_counter.cycle += 1;
			if self.frame_counter.cycle >= 29828 && self.frame_counter.irq_enabled {
				match self.frame_counter.mode {
					FrameCounterMode::FourStepMode => self.frame_counter.interrupted = true,
					FrameCounterMode::FiveStepMode => { }
				}
			}
		}
	
		self.clock_timers();
	
		match self.frame_counter.cycle {
			0 => match self.frame_counter.mode {
				FrameCounterMode::FourStepMode => { }
				FrameCounterMode::FiveStepMode => self.clock_all(),
			},
			7457 | 22371 =>
				self.clock_envelopes(),
			14913 | 37281 =>
				self.clock_all(),
			29829 => match self.frame_counter.mode {
				FrameCounterMode::FourStepMode => self.clock_all(),
				FrameCounterMode::FiveStepMode => { }
			},
			_ => { }
		}
	
		if self.frame_counter.cycle == self.frame_counter.mode as u16 {
			self.frame_counter.cycle = 0;
		}

		if (self.sample_count as usize) < self.samples.len() {
			let sample = PULSE_LOOKUP[(self.pulse1.output() + self.pulse2.output()) as usize] +
					TND_LOOKUP[(3 * self.triangle.output() + 2 * self.noise.output() + self.dmc.output()) as usize];

			self.samples[self.sample_count as usize] = self.apply_filters(sample);
			self.sample_count += 1;
		}

		self.frame_counter.interrupted || self.dmc.interrupted
	}
	
	
	fn apply_filters(&mut self, mut sample: f32) -> f32 {
		for filter in self.filters.iter_mut() {
			sample = filter.apply(sample);
		}
		sample
	}


	pub fn new(mapper: Rc<RefCell<dyn Mapper>>) -> Apu {
		let filters: Vec<Box<dyn Filter>> = vec!(
				Box::new(LowPassFilter::new(0.815686)),
//				Box::new(HighPassFilter::new(0.996039)),
				Box::new(HighPassFilter::new(0.999835)),
		);

		Apu {
			pulse1: Pulse::new(true),
			pulse2: Pulse::new(false),
			triangle: Triangle::new(),
			noise: Noise::new(),
			dmc: Dmc::new(mapper),
			frame_counter: FrameCounter::new(),
			filters,
			samples: [0.0; 29467],
			sample_count: 0,
		}
	}
	
	
	pub fn read_reg_ubyte(&mut self, address: u16) -> u8 {
		if address != REG_STATUS {
			println!("Unsupported APU register read from address ${:04X}", address);
			return 0;
		}

		let mut result = 0;
		if self.dmc.interrupted {
			result |= 0b10000000;
		}
		if self.frame_counter.interrupted {
			result |= 0b01000000;
			self.frame_counter.interrupted = false;
		}
	
		if self.dmc.bytes_left > 0 {
			result |= 0b00010000;
		}
		if self.noise.length_counter > 0 {
			result |= 0b00001000;
		}
		if self.triangle.length_counter > 0 {
			result |= 0b00000100;
		}
		if self.pulse2.length_counter > 0 {
			result |= 0b00000010;
		}
		if self.pulse1.length_counter > 0 {
			result |= 0b00000001;
		}

		result
	}
	
	pub fn write_reg_ubyte(&mut self, address: u16, value: u8) {
		match address {
			REG_PULSE_1_CONTROL => self.pulse1.reg_control_write(value),
			REG_PULSE_1_SWEEP => self.pulse1.reg_sweep_write(value),
			REG_PULSE_1_TIMER => self.pulse1.reg_timer_write(value),
			REG_PULSE_1_LENGTH => self.pulse1.reg_length_write(value),
			REG_PULSE_2_CONTROL => self.pulse2.reg_control_write(value),
			REG_PULSE_2_SWEEP => self.pulse2.reg_sweep_write(value),
			REG_PULSE_2_TIMER => self.pulse2.reg_timer_write(value),
			REG_PULSE_2_LENGTH => self.pulse2.reg_length_write(value),
			REG_TRIANGLE_LINEAR => self.triangle.reg_linear_write(value),
			REG_TRIANGLE_TIMER => self.triangle.reg_timer_write(value),
			REG_TRIANGLE_LENGTH => self.triangle.reg_length_write(value),
			REG_NOISE_ENVELOPE => self.noise.envelope.reg_write(value),
			REG_NOISE_PERIOD => self.noise.reg_period_write(value),
			REG_NOISE_LENGTH => self.noise.reg_length_write(value),
			REG_DMC_CONTROL => self.dmc.reg_control_write(value),
			REG_DMC_LOAD => self.dmc.reg_load_write(value),
			REG_DMC_ADDRESS => self.dmc.reg_address_write(value),
			REG_DMC_LENGTH => self.dmc.reg_length_write(value),
			REG_STATUS => self.reg_status_write(value),
			REG_FRAME_COUNTER => self.reg_frame_counter_write(value),
			_ => println!("Unsupported APU register write at address ${:04X}, value {:02X}", address, value),
		};
	}
	
	
	fn reg_status_write(&mut self, value: u8) {
		if value & 0b00000001 == 0 {
			self.pulse1.length_counter = 0;
		}
		if value & 0b00000010 == 0 {
			self.pulse2.length_counter = 0;
		}
		if value & 0b00000100 == 0 {
			self.triangle.length_counter = 0;
		}
		if value & 0b00001000 == 0 {
			self.noise.length_counter = 0;
		}
		if value & 0b00010000 == 0 {
			self.dmc.bytes_left = 0;
		}
	}
	
	fn reg_frame_counter_write(&mut self, value: u8) {
		self.frame_counter.mode = if value & 0x80 == 0 { FrameCounterMode::FourStepMode } else { FrameCounterMode::FiveStepMode };
		self.frame_counter.irq_enabled = value & 0x40 == 0;
		self.frame_counter.reset_counter = 3 + (self.frame_counter.cycle as u8 & 1);
	}
	
}