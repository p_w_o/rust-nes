use std::cell::RefCell;
use std::fs::File;
use std::num::Wrapping;
use std::rc::Rc;

use super::mapper::Mapper;
use super::memory::Memory;
use super::apu::Apu;
use super::ppu::Ppu;
use super::pad::Pad;
use super::state::{self, State};


pub struct Cpu {
	pub mem: CpuMemory,
	ram: Vec<u8>,

	a: u8,          // accumulator
	x: u8,          // index register x
	y: u8,          // index register y
	pub pc: u16,    // program counter
	s: u8,          // stack pointer
	p: u8,          // state register

	pub dma_count: u16,
	dma_source: u16,

	pub nmi: bool,  // whether NMI was signaled
	pub irq: bool,  // whether IRQ was signaled
	cycle: u64,     // cycles executed
}

#[derive(Copy, Clone, Debug)]
enum Flag {
	Carry,
	Zero,
	Interrupt,
	DecimalMode,
	Break,
	Unused,
	Overflow,
	Negative,
}

#[derive(Copy, Clone, Debug)]
enum Mode {
	Absolute,
	AbsoluteX,
	AbsoluteY,
	Accumulator,
	Immediate,
	Implied,
	Indirect,
	IndirectX,
	IndirectPlusY,
	Relative,
	ZeroPage,
	ZeroPageX,
	ZeroPageY,
}

#[derive(Debug)]
struct Instruction {
	opcode: u8,
	name: &'static str,
	mode: Mode,
	cycles: u8,
	page_boundary_cycles: u8,
}


const INSTRUCTIONS: [Instruction; 256] = [
		Instruction { opcode: 0x00, name: "BRK", mode: Mode::Immediate, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x01, name: "ORA", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x02, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x03, name: "*SLO", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x04, name: "*IGN", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x05, name: "ORA", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x06, name: "ASL", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x07, name: "*SLO", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x08, name: "PHP", mode: Mode::Implied, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x09, name: "ORA", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x0A, name: "ASL", mode: Mode::Accumulator, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x0B, name: "*ANC", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x0C, name: "*IGN", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x0D, name: "ORA", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x0E, name: "ASL", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x03, name: "*SLO", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x10, name: "BPL", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x11, name: "ORA", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0x12, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x13, name: "*SLO", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x14, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x15, name: "ORA", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x16, name: "ASL", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x03, name: "*SLO", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x18, name: "CLC", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x19, name: "ORA", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x1A, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x1B, name: "*SLO", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x1C, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x1D, name: "ORA", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x1E, name: "ASL", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x1F, name: "*SLO", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x20, name: "JSR", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x21, name: "AND", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x22, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x23, name: "*RLA", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x24, name: "BIT", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x25, name: "AND", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x26, name: "ROL", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x27, name: "*RLA", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x28, name: "PLP", mode: Mode::Implied, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x29, name: "AND", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2A, name: "ROL", mode: Mode::Accumulator, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2B, name: "*ANC", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2C, name: "BIT", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2D, name: "AND", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2E, name: "ROL", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x2F, name: "*RLA", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x30, name: "BMI", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x31, name: "AND", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0x32, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x33, name: "*RLA", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x34, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x35, name: "AND", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x36, name: "ROL", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x37, name: "*RLA", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x38, name: "SEC", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x39, name: "AND", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x3A, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x3B, name: "*RLA", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x3C, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x3D, name: "AND", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x3E, name: "ROL", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x3F, name: "*RLA", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x40, name: "RTI", mode: Mode::Implied, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x41, name: "EOR", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x42, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x43, name: "*SRE", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x44, name: "*IGN", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x45, name: "EOR", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x46, name: "LSR", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x47, name: "*SRE", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x48, name: "PHA", mode: Mode::Implied, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x49, name: "EOR", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4A, name: "LSR", mode: Mode::Accumulator, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4B, name: "*ALR", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4C, name: "JMP", mode: Mode::Absolute, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4D, name: "EOR", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4E, name: "LSR", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x4F, name: "*SRE", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x50, name: "BVC", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x51, name: "EOR", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0x52, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x53, name: "*SRE", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x54, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x55, name: "EOR", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x56, name: "LSR", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x57, name: "*SRE", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x58, name: "CLI", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x59, name: "EOR", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x5A, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x5B, name: "*SRE", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x5C, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x5D, name: "EOR", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x5E, name: "LSR", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x5B, name: "*SRE", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x60, name: "RTS", mode: Mode::Implied, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x61, name: "ADC", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x62, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x63, name: "*RRA", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x64, name: "*IGN", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x65, name: "ADC", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x66, name: "ROR", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x67, name: "*RRA", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x68, name: "PLA", mode: Mode::Implied, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x69, name: "ADC", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x6A, name: "ROR", mode: Mode::Accumulator, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x6B, name: "*ARR", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x6C, name: "JMP", mode: Mode::Indirect, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x6D, name: "ADC", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x6E, name: "ROR", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x63, name: "*RRA", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x70, name: "BVS", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x71, name: "ADC", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0x72, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x73, name: "*RRA", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0x74, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x75, name: "ADC", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x76, name: "ROR", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x77, name: "*RRA", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x78, name: "SEI", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x79, name: "ADC", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x7A, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x7B, name: "*RRA", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x7C, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x7D, name: "ADC", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0x7E, name: "ROR", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x7F, name: "*RRA", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0x80, name: "*SKB", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x81, name: "STA", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x82, name: "*SKB", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x83, name: "*SAX", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x84, name: "STY", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x85, name: "STA", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x86, name: "STX", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x87, name: "*SAX", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0x88, name: "DEY", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x89, name: "*SKB", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8A, name: "TXA", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8B, name: "*XAA", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8C, name: "STY", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8D, name: "STA", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8E, name: "STX", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x8F, name: "*SAX", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x90, name: "BCC", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x91, name: "STA", mode: Mode::IndirectPlusY, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0x92, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x93, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x94, name: "STY", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x95, name: "STA", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x96, name: "STX", mode: Mode::ZeroPageY, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x97, name: "*SAX", mode: Mode::ZeroPageY, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0x98, name: "TYA", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x99, name: "STA", mode: Mode::AbsoluteY, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9A, name: "TXS", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9B, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9C, name: "*SHY", mode: Mode::AbsoluteX, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9D, name: "STA", mode: Mode::AbsoluteX, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9E, name: "*SHX", mode: Mode::AbsoluteY, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0x9F, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA0, name: "LDY", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA1, name: "LDA", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA2, name: "LDX", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA3, name: "*LAX", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA4, name: "LDY", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA5, name: "LDA", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA6, name: "LDX", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA7, name: "*LAX", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA8, name: "TAY", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xA9, name: "LDA", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAA, name: "TAX", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAB, name: "*LAX", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAC, name: "LDY", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAD, name: "LDA", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAE, name: "LDX", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xAF, name: "*LAX", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB0, name: "BCS", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB1, name: "LDA", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0xB2, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB3, name: "*LAX", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB4, name: "LDY", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB5, name: "LDA", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB6, name: "LDX", mode: Mode::ZeroPageY, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB7, name: "*LAX", mode: Mode::ZeroPageY, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB8, name: "CLV", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xB9, name: "LDA", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xBA, name: "TSX", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xBB, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0xBC, name: "LDY", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xBD, name: "LDA", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xBE, name: "LDX", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xBF, name: "*LAX", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC0, name: "CPY", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC1, name: "CMP", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC2, name: "*SKB", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC3, name: "*DCP", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC4, name: "CPY", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC5, name: "CMP", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC6, name: "DEC", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC7, name: "*DCP", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC8, name: "INY", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xC9, name: "CMP", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCA, name: "DEX", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCB, name: "*AXS", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCC, name: "CPY", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCD, name: "CMP", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCE, name: "DEC", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xCF, name: "*DCP", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD0, name: "BNE", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD1, name: "CMP", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0xD2, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD3, name: "*DCP", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD4, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD5, name: "CMP", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD6, name: "DEC", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD7, name: "*DCP", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD8, name: "CLD", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xD9, name: "CMP", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xDA, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xDB, name: "*DCP", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0xDC, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xDD, name: "CMP", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xDE, name: "DEC", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0xDF, name: "*DCP", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE0, name: "CPX", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE1, name: "SBC", mode: Mode::IndirectX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE2, name: "*SKB", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE3, name: "*ISC", mode: Mode::IndirectX, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE4, name: "CPX", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE5, name: "SBC", mode: Mode::ZeroPage, cycles: 3, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE6, name: "INC", mode: Mode::ZeroPage, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE7, name: "*ISC", mode: Mode::ZeroPage, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE8, name: "INX", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xE9, name: "SBC", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xEA, name: "NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xEB, name: "*SBC", mode: Mode::Immediate, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xEC, name: "CPX", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xED, name: "SBC", mode: Mode::Absolute, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xEE, name: "INC", mode: Mode::Absolute, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xEF, name: "*ISC", mode: Mode::Absolute, cycles: 5, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF0, name: "BEQ", mode: Mode::Relative, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF1, name: "SBC", mode: Mode::IndirectPlusY, cycles: 5, page_boundary_cycles: 1 },
		Instruction { opcode: 0xF2, name: "???", mode: Mode::Implied, cycles: 0, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF3, name: "*ISC", mode: Mode::IndirectPlusY, cycles: 8, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF4, name: "*IGN", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF5, name: "SBC", mode: Mode::ZeroPageX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF6, name: "INC", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF7, name: "*ISC", mode: Mode::ZeroPageX, cycles: 6, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF8, name: "SED", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xF9, name: "SBC", mode: Mode::AbsoluteY, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xFA, name: "*NOP", mode: Mode::Implied, cycles: 2, page_boundary_cycles: 0 },
		Instruction { opcode: 0xFB, name: "*ISC", mode: Mode::AbsoluteY, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0xFC, name: "*IGN", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 0 },
		Instruction { opcode: 0xFD, name: "SBC", mode: Mode::AbsoluteX, cycles: 4, page_boundary_cycles: 1 },
		Instruction { opcode: 0xFE, name: "INC", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
		Instruction { opcode: 0xFF, name: "*ISC", mode: Mode::AbsoluteX, cycles: 7, page_boundary_cycles: 0 },
];


impl Cpu {
	pub fn new(mut memory: CpuMemory) -> Cpu {
		let pc = memory.read_ushort(0xFFFC);
		Cpu {
			mem: memory,
			ram: vec![0; 2 * 1024],
			a: 0,
			x: 0,
			y: 0,
			pc,
			s: 0xFD,
			p: (1 << Flag::Interrupt as u8) | (1 << Flag::Unused as u8),
			dma_count: 0,
			dma_source: 0,
			nmi: false,
			irq: false,
			cycle: 0,
		}
	}


	pub fn emulate(&mut self) -> u64 {
		if self.dma_count > 0 {
			let value = self.read_ubyte(self.dma_source);
			self.write_ubyte(0x2004, value);
			self.dma_source += 1;
			self.dma_count -= 1;
			self.cycle += 2;
			return 2;
		}

		if self.nmi {
			self.push_ushort(self.pc);
			self.push_ubyte(self.p);
			self.pc = self.read_ushort(0xFFFA);
			self.nmi = false;
		}

		let instruction = self.read_instruction();
		let (address, extra_cycles) = self.infer_address(&instruction);

		let start = self.cycle;

		self.pc += instruction.get_size();
		self.cycle += (instruction.cycles + extra_cycles) as u64;

		self.emulate_instruction(instruction, address);

		if self.irq && !self.get_flag(Flag::Interrupt) {
			self.irq = false;
			self.push_ushort(self.pc);
			self.push_ubyte(self.p);
			self.pc = self.read_ushort(0xFFFE);
			self.set_flag(Flag::Interrupt, true);
		}

		self.cycle - start
	}


	fn emulate_instruction(&mut self, instruction: &Instruction, address: u16) {
		match instruction.opcode {
			0x00 => {                                                   // BRK
				self.push_ushort(self.pc);
				self.push_ubyte(self.p | 0x30);
				self.set_flag(Flag::Interrupt, true);
				self.pc = self.read_ushort(0xFFFE);
			}
			0x01 | 0x05 | 0x09 | 0x0D | 0x11 | 0x15 | 0x19 | 0x1D => {  // ORA
				self.a |= self.read_ubyte(address);
				self.update_cmp_flags(self.a);
			}
			0x03 | 0x07 | 0x0F | 0x13 | 0x17 | 0x1B | 0x1F => {         //*SLO
				let operand = self.read_ubyte(address);
				let value = operand << 1;
				self.write_ubyte(address, value);
				self.set_flag(Flag::Carry, operand & 0x80 != 0);
				self.a |= value;
				self.update_cmp_flags(self.a);
			}
			0x06 | 0x0E | 0x16 | 0x1E => {                              // ASL (mem)
				let operand = self.read_ubyte(address);
				let value = operand << 1;
				self.write_ubyte(address, value);
				self.update_cmp_flags(value);
				self.set_flag(Flag::Carry, operand & 0x80 != 0);
			}
			0x08 =>                                                     // PHP
				self.push_ubyte(self.p | 0x10),
			0x0A => {                                                   // ASL (A)
				self.set_flag(Flag::Carry, self.a & 0x80 != 0);
				self.a <<= 1;
				self.update_cmp_flags(self.a);
			}
			0x0B | 0x2B => {                                            //*ANC
				self.a &= self.read_ubyte(address);
				self.update_cmp_flags(self.a);
				self.set_flag(Flag::Carry, self.get_flag(Flag::Negative));
			}
			0x04 | 0x0C | 0x14 | 0x1C | 0x34 | 0x3C | 0x44 | 0x54 |
			0x5C | 0x64 | 0x74 | 0x7C | 0xD4 | 0xDC | 0xF4 | 0xFC  => { //*IGN
				self.read_ubyte(address);
			}
			0x10 =>                                                     // BPL
				if !self.get_flag(Flag::Negative) {
					self.branch_to(address);
				},
			0x18 =>                                                     // CLC
				self.set_flag(Flag::Carry, false),
			0x20 => {                                                   // JSR
				self.push_ushort(self.pc - 1);
				self.pc = address;
			}
			0x21 | 0x25 | 0x29 | 0x2D | 0x31 | 0x35 | 0x39 | 0x3D => {  // AND
				self.a &= self.read_ubyte(address);
				self.update_cmp_flags(self.a);
			}
			0x23 | 0x27 | 0x2F | 0x33 | 0x37 | 0x3B | 0x3F => {         //*RLA
				let operand = self.read_ubyte(address);
				let result = (operand << 1) | if self.get_flag(Flag::Carry) { 1 } else { 0 };
				self.write_ubyte(address, result);
				self.set_flag(Flag::Carry, operand & 0x80 != 0);
				self.a &= result;
				self.update_cmp_flags(self.a);
			}
			0x24 | 0x2C => {                                            // BIT
				let operand = self.read_ubyte(address);
				self.set_flag(Flag::Negative, operand & 0x80 != 0);
				self.set_flag(Flag::Overflow, operand & 0x40 != 0);
				self.set_flag(Flag::Zero, operand & self.a == 0);
			}
			0x26 | 0x2E | 0x36 | 0x3E => {                              // ROL (mem)
				let operand = self.read_ubyte(address);
				let value = operand << 1 | if self.get_flag(Flag::Carry) { 1 } else { 0 };
				self.write_ubyte(address, value);
				self.update_cmp_flags(value);
				self.set_flag(Flag::Carry, operand & 0x80 != 0);
			}
			0x28 => {                                                   // PLP
				self.p = self.pop_ubyte();
				self.set_flag(Flag::Break, false);
				self.set_flag(Flag::Unused, true);
			}
			0x2A => {                                                   // ROL (A)
				let operand = self.a;
				self.a = operand << 1 | if self.get_flag(Flag::Carry) { 1 } else { 0 };
				self.update_cmp_flags(self.a);
				self.set_flag(Flag::Carry, operand & 0x80 != 0);
			}
			0x30 =>                                                     // BMI
				if self.get_flag(Flag::Negative) {
					self.branch_to(address);
				},
			0x38 =>                                                     // SEC
				self.set_flag(Flag::Carry, true),
			0x40 => {                                                   // RTI
				self.p = self.pop_ubyte();
				self.set_flag(Flag::Break, false);
				self.set_flag(Flag::Unused, true);
				self.pc = self.pop_ushort();
			}
			0x41 | 0x45 | 0x49 | 0x4D | 0x51 | 0x55 | 0x59 | 0x5D => {  // EOR
				self.a ^= self.read_ubyte(address);
				self.update_cmp_flags(self.a);
			}
			0x43 | 0x47 | 0x4F | 0x53 | 0x57 | 0x5B | 0x5F => {         //*SRE
				let operand = self.read_ubyte(address);
				let result = operand >> 1;
				self.write_ubyte(address, result);
				self.set_flag(Flag::Carry, operand & 1 != 0);
				self.a ^= result;
				self.update_cmp_flags(self.a);
			}
			0x46 | 0x4E | 0x56 | 0x5E => {                              // LSR (mem)
				let operand = self.read_ubyte(address);
				let value = operand >> 1;
				self.update_cmp_flags(value);
				self.write_ubyte(address, value);
				self.set_flag(Flag::Carry, operand & 1 != 0);
			}
			0x48 =>                                                     // PHA
				self.push_ubyte(self.a),
			0x4A => {                                                   // LSR (A)
				self.set_flag(Flag::Carry, self.a & 1 != 0);
				self.a >>= 1;
				self.update_cmp_flags(self.a);
			}
			0x4B => {                                                   //*ALR
				self.a &= self.read_ubyte(address);
				self.set_flag(Flag::Carry, self.a & 1 != 0);
				self.a >>= 1;
				self.update_cmp_flags(self.a);
			}
			0x4C | 0x6C =>                                              // JMP
				self.pc = address,
			0x50 =>                                                     // BVC
				if !self.get_flag(Flag::Overflow) {
					self.branch_to(address);
				}
			0x58 =>                                                     // CLI
				self.set_flag(Flag::Interrupt, false),
			0x60 =>                                                     // RTS
				self.pc = (Wrapping(self.pop_ushort()) + Wrapping(1)).0,
			0x61 | 0x65 | 0x69 | 0x6D | 0x71 | 0x75 | 0x79 | 0x7D => {  // ADC
				let operand = self.read_ubyte(address);
				let temp = self.a as u16 + operand as u16 + if self.get_flag(Flag::Carry) { 1 } else { 0 };
				let result = temp as u8;
				self.update_cmp_flags(result);
				self.set_flag(Flag::Carry, temp & 0x100 != 0);
				self.set_flag(Flag::Overflow, ((self.a ^ operand) & 0x80) == 0 && ((self.a ^ result) & 0x80) != 0);
				self.a = result;
			}
			0x63 | 0x67 | 0x6F | 0x73 | 0x77 | 0x7B | 0x7F => {         //*RRA
				let operand = self.read_ubyte(address);
				let operand2 = (operand >> 1) | if self.get_flag(Flag::Carry) { 0x80 } else { 0 };
				self.write_ubyte(address, operand2);

				let result = self.a as u16 + operand2 as u16 + (operand & 1) as u16;
				self.set_flag(Flag::Carry, result & 0x100 != 0);
				self.set_flag(Flag::Overflow, ((self.a ^ operand2) & 0x80) == 0 && ((self.a ^ result as u8) & 0x80) != 0);

				self.a = result as u8;
				self.update_cmp_flags(self.a);
			}
			0x66 | 0x6E | 0x76 | 0x7E => {                              // ROR (mem)
				let operand = self.read_ubyte(address);
				let value = (operand >> 1) | if self.get_flag(Flag::Carry) { 0x80 } else { 0x00 };
				self.update_cmp_flags(value);
				self.write_ubyte(address, value);
				self.set_flag(Flag::Carry, operand & 1 != 0);
			}
			0x68 => {                                                   // PLA
				self.a = self.pop_ubyte();
				self.update_cmp_flags(self.a);
			}
			0x6A => {                                                   // ROR (A)
				let operand = self.a;
				self.a = (operand >> 1) | if self.get_flag(Flag::Carry) { 0x80 } else { 0x00 };
				self.update_cmp_flags(self.a);
				self.set_flag(Flag::Carry, operand & 1 != 0);
			}
			0x6B => {                                                   //*ARR
				self.a &= self.read_ubyte(address);
				self.a = self.a >> 1 | if self.get_flag(Flag::Carry) { 0x80 } else { 0x00 };
				self.update_cmp_flags(self.a);
				self.set_flag(Flag::Carry, self.a & 0x40 != 0);
				self.set_flag(Flag::Overflow, (self.a & 0x40 != 0) != (self.a & 0x20 != 0));
			}
			0x70 =>                                                     // BVS
				if self.get_flag(Flag::Overflow) {
					self.branch_to(address);
				},
			0x78 =>                                                     // SEI
				self.set_flag(Flag::Interrupt, true),
			0x81 | 0x85 | 0x8D | 0x91 | 0x95 | 0x99 | 0x9D =>           // STA
				self.write_ubyte(address, self.a),
			0x83 | 0x87 | 0x8F | 0x97 =>                                //*SAX
				self.write_ubyte(address, self.a & self.x),
			0x84 | 0x8C | 0x94 =>                                       // STY
				self.write_ubyte(address, self.y),
			0x86 | 0x8E | 0x96 =>                                       // STX
				self.write_ubyte(address, self.x),
			0x88 => {                                                   // DEY
				self.y = (Wrapping(self.y) - Wrapping(1)).0;
				self.update_cmp_flags(self.y);
			}
			0x8A => {                                                   // TXA
				self.a = self.x;
				self.update_cmp_flags(self.a);
			}
			0x8B => {                                                   //*XAA
				self.a &= self.x & self.read_ubyte(address);
				self.update_cmp_flags(self.a);
			}
			0x90 =>                                                     // BCC
				if !self.get_flag(Flag::Carry) {
					self.branch_to(address);
				},
			0x98 => {                                                   // TYA
				self.a = self.y;
				self.update_cmp_flags(self.a);
			}
			0x9A =>                                                     // TXS
				self.s = self.x,
			0x9C => {                                                   //*SHY
				let result = (Wrapping(self.read_ubyte(self.pc - instruction.get_size() + 2)) + Wrapping(1)).0 & self.y;
				let target = if (address as u8) < self.x { ((result as u16) << 8) | (address & 0xFF) } else { address };
				self.write_ubyte(target, result);
			}
			0x9E => {                                                   //*SHX
				let result = (Wrapping(self.read_ubyte(self.pc - instruction.get_size() + 2)) + Wrapping(1)).0 & self.x;
				let target = if (address as u8) < self.y { ((result as u16) << 8) | (address & 0xFF) } else { address };
				self.write_ubyte(target, result);
			}
			0xA0 | 0xA4 | 0xAC | 0xB4 | 0xBC => {                       // LDY
				self.y = self.read_ubyte(address);
				self.update_cmp_flags(self.y);
			}
			0xA1 | 0xA5 | 0xA9 | 0xAD | 0xB1 | 0xB5 | 0xB9 | 0xBD => {  // LDA
				self.a = self.read_ubyte(address);
				self.update_cmp_flags(self.a);
			}
			0xA2 | 0xA6 | 0xAE | 0xB6 | 0xBE => {                       // LDX
				self.x = self.read_ubyte(address);
				self.update_cmp_flags(self.x);
			}
			0xA3 | 0xA7 | 0xAB | 0xAF | 0xB3 | 0xB7 | 0xBF => {         //*LAX
				self.a = self.read_ubyte(address);
				self.x = self.a;
				self.update_cmp_flags(self.x);
			}
			0xA8 => {                                                   // TAY
				self.y = self.a;
				self.update_cmp_flags(self.y);
			}
			0xAA => {                                                   // TAX
				self.x = self.a;
				self.update_cmp_flags(self.x);
			}
			0xB0 =>                                                     // BCS
				if self.get_flag(Flag::Carry) {
					self.branch_to(address);
				},
			0xB8 =>                                                     // CLV
				self.set_flag(Flag::Overflow, false),
			0xBA => {                                                   // TSX
				self.x = self.s;
				self.update_cmp_flags(self.x);
			}
			0xC0 | 0xC4 | 0xCC => {                                     // CPY
				let operand = self.read_ubyte(address);
				self.update_cmp_flags((Wrapping(self.y) - Wrapping(operand)).0);
				self.set_flag(Flag::Carry, operand <= self.y);
			}
			0xC1 | 0xC5 | 0xC9 | 0xCD |	0xD1 | 0xD5 | 0xD9 | 0xDD => {  // CMP
				let operand = self.read_ubyte(address);
				self.update_cmp_flags((Wrapping(self.a) - Wrapping(operand)).0);
				self.set_flag(Flag::Carry, operand <= self.a);
			}
			0xC3 | 0xC7 | 0xCF | 0xD3 | 0xD7 | 0xDB | 0xDF => {         //*DCP
				let value = (Wrapping(self.read_ubyte(address)) - Wrapping(1)).0;
				self.write_ubyte(address, value);
				self.update_cmp_flags((Wrapping(self.a) - Wrapping(value)).0);
				self.set_flag(Flag::Carry, value <= self.a);
			}
			0xC6 | 0xCE | 0xD6 | 0xDE => {                              // DEC
				let value = (Wrapping(self.read_ubyte(address)) - Wrapping(1)).0;
				self.update_cmp_flags(value);
				self.write_ubyte(address, value);
			}
			0xC8 => {                                                   // INY
				self.y = (Wrapping(self.y) + Wrapping(1)).0;
				self.update_cmp_flags(self.y);
			}
			0xCA => {                                                   // DEX
				self.x = (Wrapping(self.x) - Wrapping(1)).0;
				self.update_cmp_flags(self.x);
			}
			0xCB => {                                                   //*AXS
				let result = (Wrapping((self.x & self.a) as u16) - Wrapping(self.read_ubyte(address) as u16)).0;
				self.set_flag(Flag::Carry, result < 0x100);
				self.x = result as u8;
				self.update_cmp_flags(self.x);
			}
			0xD0 =>                                                     // BNE
				if !self.get_flag(Flag::Zero) {
					self.branch_to(address)
				},
			0xD8 =>                                                     // CLD
				self.set_flag(Flag::DecimalMode, false),
			0xE0 | 0xE4 | 0xEC => {                                     // CPX
				let operand = self.read_ubyte(address);
				self.update_cmp_flags((Wrapping(self.x) - Wrapping(operand)).0);
				self.set_flag(Flag::Carry, operand <= self.x);
			}
			0xE1 | 0xE5 | 0xE9 | 0xED | 0xF1 | 0xF5 | 0xF9 | 0xFD |     // SBC
			0xEB => {                                                   //*SBC
				let operand = self.read_ubyte(address);
				let result = (Wrapping(self.a as u16) - Wrapping(operand as u16) - Wrapping(if self.get_flag(Flag::Carry) { 0 } else { 1 })).0;
				self.set_flag(Flag::Carry, result <= 0xFF);
				self.set_flag(Flag::Overflow, ((self.a ^ operand) & 0x80) != 0 && ((self.a ^ result as u8) & 0x80) != 0);
				self.a = result as u8;
				self.update_cmp_flags(self.a);
			}
			0xE3 | 0xE7 | 0xEF | 0xF3 | 0xF7 | 0xFB | 0xFF => {         //*ISC
				let value = (Wrapping(self.read_ubyte(address)) + Wrapping(1)).0;
				self.write_ubyte(address, value);
				let result = (Wrapping(self.a as u16) - Wrapping(value as u16) - Wrapping(if self.get_flag(Flag::Carry) { 0 } else { 1 })).0;
				self.set_flag(Flag::Carry, result <= 0xFF);
				self.set_flag(Flag::Overflow, ((self.a ^ value) & 0x80) != 0 && ((self.a as u16 ^ result) & 0x80) != 0);
				self.a = result as u8;
				self.update_cmp_flags(self.a);
			}
			0xE6 | 0xEE | 0xF6 | 0xFE => {                              // INC
				let value = (Wrapping(self.read_ubyte(address)) + Wrapping(1)).0;
				self.update_cmp_flags(value);
				self.write_ubyte(address, value);
			}
			0xE8 => {                                                   // INX
				self.x = (Wrapping(self.x) + Wrapping(1)).0;
				self.update_cmp_flags(self.x);
			}
			0x1A | 0x3A | 0x5A | 0x7A | 0xDA | 0xFA |                   //*NOP
			0x80 | 0x82 | 0x89 | 0xC2 | 0xE2 |                          //*SKB
			0xEA => { }                                                 // NOP
			0xF0 =>                                                     // BEQ
				if self.get_flag(Flag::Zero) {
					self.branch_to(address)
				},
			0xF8 =>                                                     // SED
				self.set_flag(Flag::DecimalMode, true),
			_ =>
				panic!("Not implemented: {:?}", instruction),
		}
	}

	fn push_ubyte(&mut self, value: u8) {
		self.write_ubyte(0x0100 + self.s as u16, value);
		self.s -= 1;
	}

	fn push_ushort(&mut self, value: u16) {
		self.push_ubyte((value >> 8) as u8);
		self.push_ubyte(value as u8);
	}


	fn pop_ubyte(&mut self) -> u8 {
		self.s += 1;
		self.read_ubyte(0x0100 + self.s as u16)
	}

	fn pop_ushort(&mut self) -> u16 {
		self.pop_ubyte() as u16 | (self.pop_ubyte() as u16) << 8
	}


	fn branch_to(&mut self, address: u16) {
		self.cycle += if (address & 0xFF00) != ((self.pc - 2) & 0xFF00) { 2 } else { 1 };
		self.pc = address;
	}


	fn infer_address(&mut self, instruction: &Instruction) -> (u16, u8) {
		let mut page_cross = false;
		let address = match instruction.mode {
			Mode::Absolute => {
				self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0)
			}
			Mode::AbsoluteX => {
				let temp = self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0);
				let result = (Wrapping(temp) + Wrapping(self.x as u16)).0;
				page_cross = !pages_equal(temp, result);
				result
			}
			Mode::AbsoluteY => {
				let temp = self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0);
				let result = (Wrapping(temp) + Wrapping(self.y as u16)).0;
				page_cross = !pages_equal(temp, result);
				result
			}
			Mode::Accumulator => 0,
			Mode::Immediate =>
				(Wrapping(self.pc) + Wrapping(1)).0,
			Mode::Implied => 0,
			Mode::Indirect => {
				let temp = self.read_ushort((Wrapping(self.pc) + Wrapping(1)).0);
				self.read_ushort_bug(temp)
			}
			Mode::IndirectX => {
				let temp = (Wrapping(self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0)) + Wrapping(self.x)).0 as u16;
				self.read_ushort_bug(temp)
			}
			Mode::IndirectPlusY => {
				let temp = self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0) as u16;
				let temp = self.read_ushort_bug(temp);
				let result = (Wrapping(temp) + Wrapping(self.y as u16)).0;
				page_cross = !pages_equal(temp, result);
				result
			},
			Mode::Relative => {
				(self.pc as i32 + (self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0) as i8) as i32) as u16 + instruction.get_size()
			},
			Mode::ZeroPage =>
				self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0) as u16,
			Mode::ZeroPageX =>
				(Wrapping(self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0)) + Wrapping(self.x)).0 as u16,
			Mode::ZeroPageY =>
				(Wrapping(self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0)) + Wrapping(self.y)).0 as u16,
		};

		(address, if page_cross { instruction.page_boundary_cycles } else { 0 })
	}


	fn read_instruction(&mut self) -> &'static Instruction {
		&INSTRUCTIONS[self.read_ubyte(self.pc) as usize]
	}


	fn read_ushort_bug(&mut self, address: u16) -> u16 {
		self.read_ubyte(address) as u16 | (self.read_ubyte(address & 0xFF00 | ((Wrapping(address) + Wrapping(1)).0 & 0xFF) as u16) as u16) << 8
	}

	fn update_cmp_flags(&mut self, value: u8) {
		self.set_flag(Flag::Zero, value == 0);
		self.set_flag(Flag::Negative, value >= 128);
	}


	fn set_flag(&mut self, flag: Flag, value: bool) {
		if value {
			self.p |= (1 << flag as u8) as u8
		}
		else {
			self.p &= !((1 << flag as u8) as u8)
		}
	}

	fn get_flag(&self, flag: Flag) -> bool {
		(self.p >> flag as i32) & 1 == 1
	}



	fn print_operand(&mut self) {
		let instruction = self.read_instruction();
		let length = match instruction.mode {
			Mode::Accumulator => {
				print!("A");
				1
			},
			Mode::Implied =>
				0,
			Mode::Immediate => {
				print!("#${:02X}", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				4
			},
			Mode::Relative => {
				print!("${:04X}", (self.pc as i32 + (self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0) as i8) as i32) as u16 + instruction.get_size());
				5
			},
			Mode::ZeroPage => {
				print!("${:02X}", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				3
			},
			Mode::ZeroPageX => {
				print!("${:02X},X", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				5
			},
			Mode::ZeroPageY => {
				print!("${:02X},Y", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				5
			},
			Mode::Indirect => {
				print!("(${:04X})", self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0));
				7
			},
			Mode::IndirectX => {
				print!("(${:02X},X)", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				7
			},
			Mode::IndirectPlusY => {
				print!("(${:02X}),Y", self.read_ubyte((Wrapping(self.pc) + Wrapping(1)).0));
				7
			},
			Mode::Absolute => {
				print!("${:04X}", self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0));
				5
			},
			Mode::AbsoluteX => {
				print!("${:04X},X", self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0));
				7
			},
			Mode::AbsoluteY => {
				print!("${:04X},Y", self.mem.read_ushort((Wrapping(self.pc) + Wrapping(1)).0));
				7
			},
		};

		for _ in length..28 {
		    print!(" ");
		}
	}

	pub fn print_state(&mut self) {
		let instruction = self.read_instruction();

		print!("{:04X}  ", self.pc);
		for i in 0..3 {
			if i < instruction.get_size() {
				print!("{:02X} ", self.read_ubyte(self.pc + i));
			}
			else {
				print!("   ");
			}
		}

		print!("{:>4} ", instruction.name);
		self.print_operand();

		println!("A:{:02X} X:{:02X} Y:{:02X} P:{:02X} SP:{:02X} CYC:{:3} SL:{}",
				self.a,
				self.x,
				self.y,
				self.p,
				self.s,
				self.mem.ppu.scan_line_cycle,
				if self.mem.ppu.scan_line == 261 { -1 } else { self.mem.ppu.scan_line as i32 });
	}

}


impl Memory for Cpu {
	fn read_ubyte(&mut self, address: u16) -> u8 {
		if address < 0x2000 {		// RAM
			self.ram[(address % 0x0800) as usize]
		}
		else {
			self.mem.read_ubyte(address)
		}
	}

	fn write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x2000 {		// RAM
			self.ram[(address % 0x0800) as usize] = value;
		}
		else if address == 0x4014 {	// OAM DMA
			self.dma_count = 0x100;
			self.dma_source = (value as u16) << 8;
			self.cycle += if self.cycle & 1 == 1 { 2 } else { 1 };
		}
		else {
			self.mem.write_ubyte(address, value);
		}
	}
}


impl State for Cpu {
	fn save(&self, file: &mut File) {
		state::serialize(&self.ram, file);
		state::serialize(&self.a, file);
		state::serialize(&self.x, file);
		state::serialize(&self.y, file);
		state::serialize(&self.pc, file);
		state::serialize(&self.s, file);
		state::serialize(&self.p, file);
		state::serialize(&self.dma_count, file);
		state::serialize(&self.dma_source, file);
		state::serialize(&self.nmi, file);
		state::serialize(&self.irq, file);
		state::serialize(&self.cycle, file);
	}

	fn load(&mut self, file: &mut File) {
		self.ram = state::deserialize(file);
		self.a = state::deserialize(file);
		self.x = state::deserialize(file);
		self.y = state::deserialize(file);
		self.pc = state::deserialize(file);
		self.s = state::deserialize(file);
		self.p = state::deserialize(file);
		self.dma_count = state::deserialize(file);
		self.dma_source = state::deserialize(file);
		self.nmi = state::deserialize(file);
		self.irq = state::deserialize(file);
		self.cycle = state::deserialize(file);
	}
}


impl Instruction {
	fn get_size(&self) -> u16 {
		match self.mode {
			Mode::Absolute | Mode::AbsoluteX | Mode::AbsoluteY => 3,
			Mode::Accumulator => 1,
			Mode::Immediate => 2,
			Mode::Implied => 1,
			Mode::Indirect => 3,
			Mode::IndirectX => 2,
			Mode::IndirectPlusY => 2,
			Mode::Relative => 2,
			Mode::ZeroPage | Mode::ZeroPageX | Mode::ZeroPageY => 2,
		}
	}
}


fn pages_equal(address1: u16, address2: u16) -> bool {
	address1 & 0xFF00 == address2 & 0xFF00
}



pub struct CpuMemory {
	mapper: Rc<RefCell<dyn Mapper>>,
	pub apu: Apu,
	pub ppu: Ppu,
	pub pad0: Pad,
	pub pad1: Pad,
}


impl CpuMemory {
	pub fn new(mapper: Rc<RefCell<dyn Mapper>>, apu: Apu, ppu: Ppu) -> CpuMemory {
		CpuMemory {
			mapper,
			apu,
			ppu,
			pad0: Pad::new(),
			pad1: Pad::new(),
		}
	}
}


impl Memory for CpuMemory {
	fn read_ubyte(&mut self, address: u16) -> u8 {
		if address < 0x4000 {	// PPU registers
			self.ppu.read_reg_ubyte(address)
		}
		else if address < 0x4014 {	// APU registers
			self.apu.read_reg_ubyte(address)
		}
		else if address == 0x4014 {	// PPU DMA
			println!("PPU DMA read no yet implemented");
			0
		}
		else if address == 0x4015 {	// APU register 15
			self.apu.read_reg_ubyte(address)
		}
		else if address == 0x4016 {	// PAD 0
			self.pad0.read_ubyte()
		}
		else if address == 0x4017 {	// PAD 1 / APU register 17
			self.pad1.read_ubyte()
		}
		else if address < 0x4020 {	// APU  & I/O features: normally disabled
			println!("Rare APU & I/O features no yet implemented");
			0
		}
		else {
			self.mapper.borrow_mut().cpu_read_ubyte(address)
		}
	}

	fn write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x4000 {	// PPU registers
			self.ppu.write_reg_ubyte(address, value);
		}
		else if address < 0x4014 {	// APU registers
			self.apu.write_reg_ubyte(address, value);
		}
		else if address == 0x4014 {	// PPU DMA
			panic!("CPU should handle PPU DMA writes by itself");
		}
		else if address == 0x4015 {	// APU register 15
			self.apu.write_reg_ubyte(address, value);
		}
		else if address == 0x4016 {	// PAD 0
			self.pad0.write_ubyte(value);
			self.pad1.write_ubyte(value);
		}
		else if address == 0x4017 {	// PAD 1 / APU register 17
			self.apu.write_reg_ubyte(address, value);
		}
		else if address < 0x4020 {	// APU  & I/O features: normally disabled
			println!("Rare APU & I/O features no yet implemented");
		}
		else {
			self.mapper.borrow_mut().cpu_write_ubyte(address, value);
		}
	}
}
