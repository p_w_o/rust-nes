use super::emulator::Emulator;


pub trait Debug: Send {

	fn debug(&mut self, emulator: &mut Emulator) -> bool;

}
