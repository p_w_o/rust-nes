use std::cell::RefCell;
use std::fs::File;
use std::path::PathBuf;
use std::rc::Rc;
use std::sync::mpsc::{Receiver, Sender, self};
use std::thread;

use super::ines::Rom;
use super::mapper::Mapper;
use super::mapper::mmc1::Mmc1;
use super::mapper::uxrom::UxRom;
use super::mapper::mmc3::Mmc3;
use super::apu::Apu;
use super::cpu::{ Cpu, CpuMemory };
use super::debug::Debug;
use super::pad::Pad;
use super::ppu::{ Color, Ppu };
use super::state::{ self, State };


pub struct Emulator {
	mapper: Rc<RefCell<dyn Mapper>>,
	cpu: Cpu,
	pub cycle: u64,
	pub cpu_skip: u64,
	crc32: u32,
}


pub enum EmulatorAction {
	EmulateFrame { pad0: u8, pad1: u8 },
	Load(PathBuf),
	Save(PathBuf, Vec<Color>),
}

pub struct EmulatorOutput {
	pub picture: [Color; 256 * 240],
	pub samples: Vec<f32>,
}


impl Emulator {
	pub fn new(rom: Rom) -> Emulator {
		let crc32 = rom.crc32;
		let mapper = Emulator::create_mapper(rom);
		let apu = Apu::new(mapper.clone());
		let ppu = Ppu::new(mapper.clone());
		let cpu_mem = CpuMemory::new(mapper.clone(), apu, ppu);
		let cpu = Cpu::new(cpu_mem);
		Emulator {
			mapper,
			cpu,
			cycle: 0,
			cpu_skip: 1,
			crc32,
		}
	}


	fn create_mapper(rom: Rom) -> Rc<RefCell<dyn Mapper>> {
		match rom.header.mapper {
			0 | 1 => Rc::new(RefCell::new(Mmc1::new(rom))),
			2 => Rc::new(RefCell::new(UxRom::new(rom))),
			4 => Rc::new(RefCell::new(Mmc3::new(rom))),
			_ => panic!("Unsupported mapper {}", rom.header.mapper),
		}
	}


	pub fn emulate_frame(&mut self, debuggers: &mut [Box<dyn Debug>]) -> EmulatorOutput {
		loop {
			for debugger in debuggers.iter_mut() {
				debugger.debug(self);
			}

			if self.mapper.borrow_mut().irq() {
				self.cpu().irq = true;
			}

			if self.cycle % 3 == 0 {
				self.cpu_skip -= 1;
				if self.cpu_skip == 0 {
					self.cpu_skip = self.cpu().emulate();
				}

				if self.apu().emulate() {
					self.cpu().irq = true;
				}
			}
			let ppu_result = self.ppu().emulate();
			if ppu_result.nmi {
				self.cpu().nmi = true;
			}

			self.cycle += 1;
			if ppu_result.frame_ready {
				break;
			}
		}

		let count = self.apu().sample_count as usize;
		self.apu().sample_count = 0;
		EmulatorOutput {
			picture: self.ppu().picture,
			samples: self.apu().samples[..count].to_vec()
		}
	}


	fn apu(&mut self) -> &mut Apu {
		&mut self.cpu.mem.apu
	}

	pub fn cpu(&mut self) -> &mut Cpu {
		&mut self.cpu
	}

	pub fn ppu(&mut self) -> &mut Ppu {
		&mut self.cpu.mem.ppu
	}


	pub fn pad0(&mut self) -> &mut Pad {
		&mut self.cpu.mem.pad0
	}

	pub fn pad1(&mut self) -> &mut Pad {
		&mut self.cpu.mem.pad1
	}

	pub fn save(&self, file: &PathBuf, picture: &Vec<Color>) {
		let mut file = File::create(file).expect("Could not open file for saving");
		state::serialize(picture, &mut file);
		state::serialize(&self.crc32, &mut file);
		self.mapper.borrow().save(&mut file);
		self.cpu.save(&mut file);
		self.cpu.mem.ppu.save(&mut file);
	}

	pub fn load(&mut self, file: &PathBuf) {
		let mut file = File::open(file).expect("Could not open file for loading");
		state::deserialize::<Vec<Color>>(&mut file);
		if self.crc32 != state::deserialize(&mut file) {
			println!("Invalid save file");
			return;
		}
		self.mapper.borrow_mut().load(&mut file);
		self.cpu().load(&mut file);
		self.ppu().load(&mut file);
	}

	pub fn load_image(file: &PathBuf) -> Option<Vec<Color>> {
		File::open(file).map(|mut f| state::try_deserialize(&mut f).ok()).ok().flatten()
	}

}


pub fn new_emulator_thread(rom: Rom, mut debuggers: Vec<Box<dyn Debug>>) -> (Receiver<EmulatorOutput>, Sender<EmulatorAction>) {
	let (tx_output, rx_output) = mpsc::channel();
	let (tx_input, rx_input) = mpsc::channel();

	#[cfg(debug_assertions)]
	let stack_size = 64 * 1024 * 1024;
	#[cfg(not(debug_assertions))]
	let stack_size = 8 * 1024 * 1024;

	thread::Builder::new().name("emulator".to_string()).stack_size(stack_size).spawn(move || {
		let mut emulator = Emulator::new(rom);
		loop {
			if let Ok(action) = rx_input.recv() {
				match action {
					EmulatorAction::Load(file) => emulator.load(&file),
					EmulatorAction::Save(file, picture) => emulator.save(&file, &picture),
					EmulatorAction::EmulateFrame { pad0, pad1 } => {
						emulator.pad0().set_state(pad0);
						emulator.pad1().set_state(pad1);
						let result = emulator.emulate_frame(&mut debuggers[..]);
						tx_output.send(result).unwrap();
					}
				}
			}
			else {
				return;
			}
		}
	}).expect("Failed to spawn emulator thread");

	(rx_output, tx_input)
}