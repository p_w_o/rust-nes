
pub trait Filter {
	fn apply(&mut self, sample: f32) -> f32;
}

pub struct HighPassFilter {
	multiplier: f32,
	prev_in: f32,
	prev_out: f32,
}

impl HighPassFilter {
	pub fn new(multiplier: f32) -> HighPassFilter {
		HighPassFilter {
			multiplier,
			prev_in: 0.0,
			prev_out: 0.0,
		}
	}
}

impl Filter for HighPassFilter {
	fn apply(&mut self, sample: f32) -> f32 {
		self.prev_out = self.prev_out * self.multiplier + sample - self.prev_in;
		self.prev_in = sample;
		self.prev_out
	}
}


pub struct LowPassFilter {
	multiplier: f32,
	prev_out: f32,
}

impl Filter for LowPassFilter {
	fn apply(&mut self, sample: f32) -> f32 {
		self.prev_out = (sample - self.prev_out) * self.multiplier;
		self.prev_out
	}
}


impl LowPassFilter {
	pub fn new(multiplier: f32) -> LowPassFilter {
		LowPassFilter {
			multiplier,
			prev_out: 0.0,
		}
	}
}
