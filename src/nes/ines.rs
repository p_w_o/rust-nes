use std::fs::File;
use std::io::Read;
use std::vec::Vec;

use crate::util::crc32::Crc32;

const MAGIC: [u8; 4] = [ 78, 69, 83, 26 ];
const ZERO: [u8; 5] = [ 0, 0, 0, 0, 0 ];


#[derive(Copy, Clone, Debug)]
pub enum TvSystem {
	Ntsc,
	Pal,
}

#[derive(Copy, Clone, Debug)]
pub enum Mirroring { 
	Vertical,
	Horizontal,
	None,
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct Header {
	prg_ram_size: u32,
	prg_rom_size: u32,
	chr_size: u32,
	chr_writable: bool,
	pub mapper: u8,
	pub mirroring: Mirroring,
	persistent_mem: bool,
	trainer: bool,
	vs_unisystem: bool,
	play_choice_10: bool,
	nes2: bool,
	tv_system: TvSystem,
}

pub struct Rom {
	pub header: Header,
	pub prg_rom: Vec<u8>,
	pub chr_rom: Vec<u8>,
	pub crc32: u32,
}


impl Rom {
	pub fn read(file: &mut File) -> Result<Rom, String> {
		let mut digest = Crc32::new();
		let header = read_header(file, &mut digest)?;

		if header.trainer {
			let mut buf = [0u8; 512];
			file.read_exact(&mut buf).map_err(|_| { "Failed reading trainer data" })?;
			digest.update(&buf);
		}

		let mut prg_rom = vec![0; header.prg_rom_size as usize];
		file.read_exact(&mut prg_rom).map_err(|_| { "Failed reading PRG ROM" })?;
		digest.update(&prg_rom);

		let mut chr_rom = vec![0; if header.chr_writable { 0 } else { header.chr_size as usize }];
		file.read_exact(&mut chr_rom).map_err(|_| { "Failed reading PRG ROM" })?;
		digest.update(&chr_rom);

		Result::Ok(Rom {
			header,
			prg_rom,
			chr_rom,
			crc32: digest.value(),
		})
	}
}


fn read_header(file: &mut File, digest: &mut Crc32) -> Result<Header, String> {
	let mut buf=[0u8; 16];
	file.read_exact(&mut buf).map_err(|_| { "read failed" })?;
	digest.update(&buf);
	if MAGIC != buf[0..4] {
		return Result::Err("invalid magic numbers".to_string());
	}

	if ZERO != buf[11..16] {
		println!("Non-zero values in zero bytes");
	}

	Result::Ok(Header {
		prg_ram_size: buf[8] as u32 * 8 * 1024,
		prg_rom_size: buf[4] as u32 * 16 * 1024,
		chr_size: if buf[5] == 0 { 1 } else { buf[5] as u32 } * 8 * 1024,
		chr_writable: buf[5] == 0,
		mapper: buf[7] & 0xF0 | buf[6] >> 4,
		mirroring: infer_mirroring(buf[6]),
		persistent_mem: buf[6] & 0b0000_0010 != 0,
		trainer: buf[6] & 0b0000_0100 != 0,
		vs_unisystem: buf[7] & 0b0000_0001 != 0,
		play_choice_10: buf[7] & 0b0000_0010 != 0,
		nes2: buf[7] & 0b0000_1100 == 0b0000_1000,
		tv_system: if buf[9] & 0b0000_0001 != 0 { TvSystem::Pal } else { TvSystem::Ntsc },
	})
}


fn infer_mirroring(flags6: u8) -> Mirroring {
	if flags6 & 0b0000_1000  != 0 {
		Mirroring::None
	}
	else if flags6 & 0b0000_0001 != 0 {
		Mirroring::Vertical
	}
	else {
		Mirroring::Horizontal
	}
}
