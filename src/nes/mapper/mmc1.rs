use std::fs::File;
use serde::{Serialize, Deserialize};

use crate::nes::ines::Rom;
use crate::nes::ines::Mirroring;
use crate::nes::state::{self, State};
use super::Mapper;


#[derive(Serialize, Deserialize)]
pub struct Mmc1 {
	sr: u8,
	control: u8,
	chr_bank_0: u8,
	chr_bank_1: u8,
	prg_bank: u8,
	
	prg: Vec<u8>,
	prg_offsets: [u32; 2],
	chr: Vec<u8>,
	chr_offsets: [u32; 2],
	
	prg_ram: Vec<u8>,

	ppu_ram: Vec<u8>,
	ppu_namespace_offsets: [u32; 4],
}


impl Mapper for Mmc1 {
	
	fn ppu_peek_ubyte(&self, address: u16) -> u8 {
		if address < 0x2000 {		// CHR
			self.chr[(self.chr_offsets[(address >> 12) as usize] + (address & 0x0FFF) as u32) as usize]
		}
		else if address < 0x3F00 {	// Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize]
		}
		else {
			println!("MMC1: invalid PPU read at address ${:X}", address);
			0x00
		}
	}

	fn ppu_read_ubyte(&mut self, address: u16) -> u8 {
		self.ppu_peek_ubyte(address)
	}

	fn ppu_write_ubyte(&mut self, address: u16, value: u8) { 
		if address < 0x2000 {		// CHR
			self.chr[(self.chr_offsets[(address >> 12) as usize] + (address & 0x0FFF) as u32) as usize] = value;
		}
		else if address < 0x3F00 {	// Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize] = value;
		}
		else {
			println!("MMC1: invalid PPU write at address ${:04X}, value ${:02X}", address, value);
		}
	}

	fn cpu_read_ubyte(&self, address: u16) -> u8 { 
		if address < 0x6000 {
			println!("MMC1: unsupported PRG read from ${:04X}", address);
			0x00
		}
		else if address < 0x8000 {	// PRG RAM
			self.prg_ram[(address - 0x6000) as usize]
		}
		else {						// PRG ROM
			self.prg[self.prg_offsets[((address >> 14) & 0x01) as usize] as usize + (address & 0x3FFF) as usize]
		}
	}
	
	fn cpu_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x6000 {
			println!("MMC1: unsupported PRG write to ${:04X}, value ${:02X}", address, value);
		}
		else if address < 0x8000 {	// PRG RAM
			self.prg_ram[(address - 0x6000) as usize] = value;
		}
		else {
			self.prg_rom_write_ubyte(address, value);
		}
	}

}


impl State for Mmc1 {
	fn save(&self, file: &mut File) {
		state::serialize(&self, file);
	}

	fn load(&mut self, file: &mut File) {
		let value: Mmc1 = state::deserialize(file);
		*self = value;
	}
}


impl Mmc1 {
	pub fn new(rom: Rom) -> Mmc1 {
		let mut result = Mmc1 {
			sr: 0x80,
			control: initial_control_value(&rom),
			chr_bank_0: 0,
			chr_bank_1: 0,
			prg_bank: 0,
			prg: rom.prg_rom,
			prg_offsets: [0, 0],
			chr: if !rom.chr_rom.is_empty() { rom.chr_rom } else { vec![0; 8 * 1024] },
			chr_offsets: [0, 0],
			prg_ram: vec![0; 8 * 1024],
			ppu_ram: vec![0; 2 * 1024],
			ppu_namespace_offsets: [0, 0, 0, 0],
		};

		result.update_prg_offsets();
		result.update_chr_offsets();
		result.update_namespace_offsets();
		result
	}

	fn update_prg_offsets(&mut self) {
		self.prg_offsets = match self.control >> 2 &0b11 {
			0 | 1 => [
				(self.prg_bank & 0b11111110) as u32 * 0x4000 % self.prg.len() as u32,
				(self.prg_bank | 0b00000001) as u32 * 0x4000 % self.prg.len() as u32
			],
			2 => [
				0,
				self.prg_bank as u32 * 0x4000 % self.prg.len() as u32
			],
			3 => [
				self.prg_bank as u32 * 0x4000 % self.prg.len() as u32,
				self.prg.len() as u32 - 0x4000
			],
			_ =>
				panic!("mmc1: invalid PRG bank mode"),
		};
	}


	fn update_chr_offsets(&mut self) {
		self.chr_offsets = if self.control & 0b00010000 != 0 {
			[
				((self.chr_bank_0 as u32) << 12) % (self.chr.len() as u32),
				((self.chr_bank_1 as u32) << 12) % (self.chr.len() as u32)
			]
		}
		else {
			[
				((self.chr_bank_0 as u32 & 0b11111110 ) << 12) % (self.chr.len() as u32),
				((self.chr_bank_0 as u32 | 0b00000001 ) << 12) % (self.chr.len() as u32),
			]
		};
	}
	
	fn update_namespace_offsets(&mut self) {
		self.ppu_namespace_offsets = match self.control & 0x03 {
			0 => [0x0000, 0x0000, 0x0000, 0x0000],
			1 => [0x0400, 0x0400, 0x0400, 0x0400],
			2 => [0x0000, 0x0400, 0x0000, 0x0400],
			3 => [0x0000, 0x0000, 0x0400, 0x0400],
			_ =>
				panic!("mmc1: invalid PRG bank mode"),
		};
	}


	fn prg_rom_write_ubyte(&mut self, address: u16, value: u8) {
		if value & 0x80 != 0 {
			self.sr = 0x80;
			self.control |= 0x0C;
			self.update_prg_offsets();
			self.update_chr_offsets();
			return;
		}
	
		self.sr = self.sr >> 1 | ((value & 1) << 7);
		if self.sr & 0b00000100 != 0 {
			match address >> 13 & 0b11 {
				0 => {
					self.control = self.sr >> 3;
					self.update_prg_offsets();
					self.update_chr_offsets();
					self.update_namespace_offsets();
				},
				1 => {
					self.chr_bank_0 = self.sr >> 3;
					self.update_chr_offsets();
				},
				2 => {
					self.chr_bank_1 = self.sr >> 3;
					self.update_chr_offsets();
				},
				3 => {
					self.prg_bank = self.sr >> 3;
					self.update_prg_offsets();
				}
				_ => panic!("")
			};
			self.sr = 0x80;
		}
	}

}


fn initial_control_value(rom: &Rom) -> u8 {
	0x0C + (match rom.header.mirroring {
		Mirroring::Horizontal => 0b11,
		Mirroring::Vertical => 0b10,
		Mirroring::None => 0b00,
	})
}
