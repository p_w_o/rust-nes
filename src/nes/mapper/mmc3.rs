use std::fs::File;
use serde::{Serialize, Deserialize};

use crate::nes::ines::Rom;
use crate::nes::ines::Mirroring;
use crate::nes::state::{self, State};
use super::Mapper;


enum Bank {
	Chr0,
	Chr1,
	Chr2,
	Chr3,
	Chr4,
	Chr5,
	Prg0,
	Prg1
}


#[derive(Serialize, Deserialize)]
pub struct Mmc3 {
	bank_select: u8,
	registers: [u8; 8],

	chr_fetch_high: bool,

	irq_counter: u8,
	irq_enabled: bool,
	irq_reload_value: u8,
	irq: bool,

	prg: Vec<u8>,
	prg_offsets: [u32; 4],
	chr: Vec<u8>,
	chr_offsets: [u32; 8],

	prg_ram: Vec<u8>,

	ppu_ram: Vec<u8>,
	ppu_namespace_offsets: [u32; 4],
}


impl Mapper for Mmc3 {

	fn ppu_peek_ubyte(&self, address: u16) -> u8 {
		if address < 0x2000 {       // CHR
			self.chr[(self.chr_offsets[(address >> 10) as usize] + (address & 0x3FF) as u32) as usize]
		}
		else if address < 0x3F00 {  // Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize]
		}
		else {
			println!("MMC3: invalid PPU read at address ${:X}", address);
			0x00
		}
	}

	fn ppu_read_ubyte(&mut self, address: u16) -> u8 {
		if address < 0x2000 {       // CHR
			self.handle_scanline(address & 0x1000 != 0);
		}
		self.ppu_peek_ubyte(address)
	}

	fn ppu_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x2000 {       // CHR
			self.chr[(self.chr_offsets[(address >> 10) as usize] + (address & 0x3FF) as u32) as usize] = value;
		}
		else if address < 0x3F00 {  // Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize] = value;
		}
		else {
			println!("MMC3: invalid PPU write at address ${:04X}, value ${:02X}", address, value);
		}
	}

	fn cpu_read_ubyte(&self, address: u16) -> u8 {
		if address < 0x6000 {
			println!("MMC3: unsupported PRG read from ${:04X}", address);
			0x00
		}
		else if address < 0x8000 {  // PRG RAM
			self.prg_ram[(address - 0x6000) as usize]
		}
		else {                      // PRG ROM
			self.prg[self.prg_offsets[((address >> 13) & 0b11) as usize] as usize + (address & 0x1FFF) as usize]
		}
	}

	fn cpu_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x6000 {
			println!("MMC3: unsupported PRG write to ${:04X}, value ${:02X}", address, value);
		}
		else if address < 0x8000 {  // PRG RAM
			self.prg_ram[(address - 0x6000) as usize] = value;
		}
		else {
			self.prg_rom_write_ubyte(address, value);
		}
	}

	fn irq(&mut self) -> bool {
		if self.irq {
			self.irq = false;
			true
		}
		else {
			false
		}
	}

}


impl State for Mmc3 {
	fn save(&self, file: &mut File) {
		state::serialize(&self, file);
	}

	fn load(&mut self, file: &mut File) {
		let value: Mmc3 = state::deserialize(file);
		*self = value;
	}
}


impl Mmc3 {

	pub fn new(rom: Rom) -> Mmc3 {
		let mut result = Mmc3 {
			bank_select: 0,
			registers: [0; 8],

			chr_fetch_high: false,
			irq: false,

			irq_counter: 0,
			irq_enabled: false,
			irq_reload_value: 0,

			prg: rom.prg_rom,
			prg_offsets: [0; 4],
			chr: rom.chr_rom,
			chr_offsets: [0; 8],

			prg_ram: vec![0; 8 * 1024],

			ppu_ram: vec![0; 2 * 1024],
			ppu_namespace_offsets: [0; 4],
		};

		result.prg_offsets[3] = (result.prg.len() - 8192) as u32;
		result.update_bank_offsets();
		result.ppu_namespace_offsets[match rom.header.mirroring { Mirroring::Vertical => 1, _ => 2 }] = 0x0400;
		result.ppu_namespace_offsets[3] = 0x0400;
		result
	}


	fn handle_scanline(&mut self, high: bool) {
		if self.chr_fetch_high != high {
			if high {
				if self.irq_counter == 0 {
					self.irq_counter = self.irq_reload_value;
				}
				else {
					self.irq_counter -= 1;
					if self.irq_counter == 0 && self.irq_enabled {
						self.irq = true;
					}
				}
			}
			self.chr_fetch_high = high;
		}
	}


	fn prg_rom_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0xA000 {
			self.prg_bank_reg_write(address, value);
		}
		else if address < 0xC000 {
			self.prg_mirror_reg_write(address, value);
		}
		else if address < 0xE000 {
			self.prg_irq_reload_reg_write(address, value);
		}
		else {
			self.prg_irq_enable_reg_write(address, value);
		}
	}


	fn prg_bank_reg_write(&mut self, address: u16, value: u8) {
		if address & 1 != 0 {
			let bank = self.bank_select as usize & 0x07;
			self.registers[bank] = value;
		}
		else {
			self.bank_select = value;
		}
		self.update_bank_offsets();
	}


	fn prg_mirror_reg_write(&mut self, address: u16, value: u8) {
		if address & 1 != 0 {
			// PRG RAM protect: ignored at least for now
		}
		else {
			self.ppu_namespace_offsets[1 + (value as usize & 1)] = 0x0400;
			self.ppu_namespace_offsets[2 - (value as usize & 1)] = 0x0000;
		}
	}


	fn prg_irq_reload_reg_write(&mut self, address: u16, value: u8) {
		if address & 1 != 0 {
			self.irq_counter = 0;
		}
		else {
			self.irq_reload_value = value;
		}
	}


	fn prg_irq_enable_reg_write(&mut self, address: u16, _value: u8) {
		self.irq_enabled = address & 1 != 0;
	}


	fn update_bank_offsets(&mut self) {
		let prg_mode = self.bank_select & 0x40 != 0;
		self.prg_offsets[if prg_mode { 2 } else { 0 }] = (self.registers[Bank::Prg0 as usize] as u32 & 0x3F) << 13;
		self.prg_offsets[1] = (self.registers[Bank::Prg1 as usize] as u32 & 0x3F) << 13;
		self.prg_offsets[if prg_mode { 0 } else { 2 }] = self.prg_offsets[3] - 0x2000;

		let index_offset = if self.bank_select & 0x80 != 0 { 4 } else { 0 };
		self.chr_offsets[(    index_offset) % 8] = (self.registers[Bank::Chr0 as usize] as u32 & 0xFE) << 10;
		self.chr_offsets[(1 + index_offset) % 8] = (self.registers[Bank::Chr0 as usize] as u32 | 0x01) << 10;
		self.chr_offsets[(2 + index_offset) % 8] = (self.registers[Bank::Chr1 as usize] as u32 & 0xFE) << 10;
		self.chr_offsets[(3 + index_offset) % 8] = (self.registers[Bank::Chr1 as usize] as u32 | 0x01) << 10;
		self.chr_offsets[(4 + index_offset) % 8] = (self.registers[Bank::Chr2 as usize] as u32) << 10;
		self.chr_offsets[(5 + index_offset) % 8] = (self.registers[Bank::Chr3 as usize] as u32) << 10;
		self.chr_offsets[(6 + index_offset) % 8] = (self.registers[Bank::Chr4 as usize] as u32) << 10;
		self.chr_offsets[(7 + index_offset) % 8] = (self.registers[Bank::Chr5 as usize] as u32) << 10;
	}

}
