pub mod mmc1;
pub mod uxrom;
pub mod mmc3;

use super::state::State;


pub trait Mapper: State {

	fn cpu_read_ubyte(&self, address: u16) -> u8;

	fn cpu_read_ushort(&self, address: u16) -> u16 {
		self.cpu_read_ubyte(address) as u16 + (self.cpu_read_ubyte(address + 1) as u16) * 256
	}

	fn cpu_write_ubyte(&mut self, address: u16, value: u8);

	fn ppu_peek_ubyte(&self, address: u16) -> u8;

	fn ppu_read_ubyte(&mut self, address: u16) -> u8;

	fn ppu_write_ubyte(&mut self, address: u16, value: u8);

	fn irq(&mut self) -> bool {
		false
	}

}
