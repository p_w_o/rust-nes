use std::fs::File;
use serde::{Serialize, Deserialize};

use crate::nes::ines::Rom;
use crate::nes::ines::Mirroring;
use crate::nes::state::{self, State};
use super::Mapper;


#[derive(Serialize, Deserialize)]
pub struct UxRom {
	prg: Vec<u8>,
	prg_offsets: [u32; 2],
	chr: Vec<u8>,

	ppu_ram: Vec<u8>,
	ppu_namespace_offsets: [u32; 4],
}


impl Mapper for UxRom {

	fn ppu_peek_ubyte(&self, address: u16) -> u8 {
		if address < 0x2000 {       // CHR
			self.chr[address as usize]
		}
		else if address < 0x3F00 {  // Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize]
		}
		else {
			println!("UxRom: invalid PPU read at address ${:X}", address);
			0x00
		}
	}

	fn ppu_read_ubyte(&mut self, address: u16) -> u8 {
		self.ppu_peek_ubyte(address)
	}

	fn ppu_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x2000 {       // CHR
			self.chr[address as usize] = value;
		}
		else if address < 0x3F00 {  // Namespace
			self.ppu_ram[(self.ppu_namespace_offsets[(address >> 10) as usize & 0b11] + (address & 0x3FF) as u32) as usize] = value;
		}
		else {
			println!("UxRom: invalid PPU write at address ${:04X}, value ${:02X}", address, value);
		}
	}

	fn cpu_read_ubyte(&self, address: u16) -> u8 {
		if address < 0x8000 {
			println!("UxRom: unsupported PRG read from ${:04X}", address);
			0x00
		}
		else {                      // PRG ROM
			self.prg[self.prg_offsets[((address >> 14) & 0b1) as usize] as usize + (address & 0x3FFF) as usize]
		}
	}

	fn cpu_write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x8000 {
			println!("UxRom: unsupported PRG write to ${:04X}, value ${:02X}", address, value);
		}
		else {
			self.prg_rom_write_ubyte(address, value);
		}
	}
}


impl State for UxRom {
	fn save(&self, file: &mut File) {
		state::serialize(&self, file);
	}

	fn load(&mut self, file: &mut File) {
		let value: UxRom = state::deserialize(file);
		*self = value;
	}
}


impl UxRom {

	pub fn new(rom: Rom) -> UxRom {
		let mut result = UxRom {
			prg: rom.prg_rom,
			prg_offsets: [0; 2],
			chr: vec![0; 8 * 1024],
			ppu_ram: vec![0; 2 * 1024],
			ppu_namespace_offsets: [0; 4],
		};

		result.prg_offsets[1] = result.prg.len() as u32 - 16 * 1024;
		result.ppu_namespace_offsets[match rom.header.mirroring { Mirroring::Vertical => 1, _ => 2 }] = 0x0400;
		result.ppu_namespace_offsets[3] = 0x0400;
		result
	}


	fn prg_rom_write_ubyte(&mut self, _address: u16, value: u8) {
		self.prg_offsets[0] = (value & 0x0F) as u32 * 16 * 1024;
	}

}
