use std::num::Wrapping;


pub trait Memory {

	fn read_ubyte(&mut self, address: u16) -> u8;

	fn read_ushort(&mut self, address: u16) -> u16 {
		self.read_ubyte(address) as u16 | (self.read_ubyte(address + 1) as u16) << 8
	}


	fn write_ubyte(&mut self, address: u16, value: u8);

	fn write_ushort(&mut self, address: u16, value: u16) {
		self.write_ubyte(address, value as u8);
		self.write_ubyte((Wrapping(address) + Wrapping(1)).0, (value >> 8) as u8);
	}

}

