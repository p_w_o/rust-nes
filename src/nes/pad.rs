
pub struct Pad {
	reg_in: u8,
	reg_out: u8,
	state: u8,
}

impl Pad {
	pub fn read_ubyte(&mut self) -> u8 {
		if self.reg_in != 0 {
			self.reg_out = self.state;
			self.reg_out & 1
		}
		else {
			let result = self.reg_out & 1;
			self.reg_out >>= 1;
			result & 1
		}
	}
	
	pub fn write_ubyte(&mut self, value: u8) {
		self.reg_in = value & 1;
		self.reg_out = self.state;
	}

	pub fn set_state(&mut self, value: u8) {
		self.state = value;
	}
	
	
	pub fn new() -> Pad {
		Pad {
			reg_in: 0,
			reg_out: 0,
			state: 0,
		}
	}
}
