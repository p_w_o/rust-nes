use std::fs::File;

use serde::{Serialize, Deserialize};

use crate::nes::state::{self, State};

use super::mapper::Mapper;
use std::cell::RefCell;
use std::rc::Rc;
use std::num::Wrapping;


const SPRITE_EMPTY: Sprite = Sprite { y: 0xFF, tile: 0xFF, attributes: 0xFF, x: 0xFF, index: 0xFF, low: 0x00, high: 0x00 };

const SCAN_LINE_POST_RENDER: u16 = 240;
const SCAN_LINE_VBLANK_0 : u16 = 241;
const SCAN_LINE_PRE_RENDER : u16 = 261;

const OAM_SPRITE_Y: usize = 0;
const OAM_SPRITE_TILE: usize = 1;
const OAM_SPRITE_ATTRIBUTES: usize = 2;
const OAM_SPRITE_X: usize = 3;

const REG_PPUCTRL: u16 = 0x2000;
const REG_PPUMASK: u16 = 0x2001;
const REG_PPUSTATUS: u16 = 0x2002;
const REG_OAMADDR: u16 = 0x2003;
const REG_OAMDATA: u16 = 0x2004;
const REG_PPUSCROLL: u16 = 0x2005;
const REG_PPUADDR: u16 = 0x2006;
const REG_PPUDATA: u16 = 0x2007;


#[derive(Copy, Clone, Debug)]
pub struct PpuResult {
	pub frame_ready: bool,
	pub nmi: bool,
}


#[derive(Copy, Clone, Debug)]
enum Flag {
	SpriteOverflow = 5,
	SpriteZeroHit = 6,
	VerticalBlank = 7,
}


#[derive(PartialEq, Eq, Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Color {
	pub r: u8,
	pub g: u8,
	pub b: u8,
}

impl Color {
	pub fn new(value: u8) -> Color {
		match value {
			0x00 => Color { r: 0x65, g: 0x65, b: 0x65 },
			0x01 => Color { r: 0x00, g: 0x2B, b: 0x9B },
			0x02 => Color { r: 0x11, g: 0x0E, b: 0xC0 },
			0x03 => Color { r: 0x3F, g: 0x00, b: 0xBC },
			0x04 => Color { r: 0x66, g: 0x00, b: 0x8F },
			0x05 => Color { r: 0x7B, g: 0x00, b: 0x45 },
			0x06 => Color { r: 0x79, g: 0x01, b: 0x00 },
			0x07 => Color { r: 0x60, g: 0x1C, b: 0x00 },
			0x08 => Color { r: 0x36, g: 0x38, b: 0x00 },
			0x09 => Color { r: 0x08, g: 0x4F, b: 0x00 },
			0x0A => Color { r: 0x00, g: 0x5A, b: 0x00 },
			0x0B => Color { r: 0x00, g: 0x57, b: 0x02 },
			0x0C => Color { r: 0x00, g: 0x45, b: 0x55 },
			0x0D => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x0E => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x0F => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x10 => Color { r: 0xAE, g: 0xAE, b: 0xAE },
			0x11 => Color { r: 0x07, g: 0x61, b: 0xF5 },
			0x12 => Color { r: 0x3E, g: 0x3B, b: 0xFF },
			0x13 => Color { r: 0x7C, g: 0x1D, b: 0xFF },
			0x14 => Color { r: 0xAF, g: 0x0E, b: 0xE5 },
			0x15 => Color { r: 0xCB, g: 0x13, b: 0x83 },
			0x16 => Color { r: 0xC8, g: 0x2A, b: 0x15 },
			0x17 => Color { r: 0xA7, g: 0x4D, b: 0x00 },
			0x18 => Color { r: 0x6F, g: 0x72, b: 0x00 },
			0x19 => Color { r: 0x32, g: 0x91, b: 0x00 },
			0x1A => Color { r: 0x00, g: 0x9F, b: 0x00 },
			0x1B => Color { r: 0x00, g: 0x9B, b: 0x2A },
			0x1C => Color { r: 0x00, g: 0x84, b: 0x98 },
			0x1D => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x1E => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x1F => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x20 => Color { r: 0xFF, g: 0xFF, b: 0xFF },
			0x21 => Color { r: 0x56, g: 0xB1, b: 0xFF },
			0x22 => Color { r: 0x8E, g: 0x8B, b: 0xFF },
			0x23 => Color { r: 0xCC, g: 0x6C, b: 0xFF },
			0x24 => Color { r: 0xFF, g: 0x5D, b: 0xFF },
			0x25 => Color { r: 0xFF, g: 0x62, b: 0xD4 },
			0x26 => Color { r: 0xFF, g: 0x79, b: 0x64 },
			0x27 => Color { r: 0xF8, g: 0x9D, b: 0x06 },
			0x28 => Color { r: 0xC0, g: 0xC3, b: 0x00 },
			0x29 => Color { r: 0x81, g: 0xE2, b: 0x00 },
			0x2A => Color { r: 0x4D, g: 0xF1, b: 0x16 },
			0x2B => Color { r: 0x30, g: 0xEC, b: 0x7A },
			0x2C => Color { r: 0x34, g: 0xD5, b: 0xEA },
			0x2D => Color { r: 0x4E, g: 0x4E, b: 0x4E },
			0x2E => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x2F => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x30 => Color { r: 0xFF, g: 0xFF, b: 0xFF },
			0x31 => Color { r: 0xBA, g: 0xDF, b: 0xFF },
			0x32 => Color { r: 0xD1, g: 0xD0, b: 0xFF },
			0x33 => Color { r: 0xEB, g: 0xC3, b: 0xFF },
			0x34 => Color { r: 0xFF, g: 0xBD, b: 0xFF },
			0x35 => Color { r: 0xFF, g: 0xBF, b: 0xEE },
			0x36 => Color { r: 0xFF, g: 0xC8, b: 0xC0 },
			0x37 => Color { r: 0xFC, g: 0xD7, b: 0x99 },
			0x38 => Color { r: 0xE5, g: 0xE7, b: 0x84 },
			0x39 => Color { r: 0xCC, g: 0xF3, b: 0x87 },
			0x3A => Color { r: 0xB6, g: 0xF9, b: 0xA0 },
			0x3B => Color { r: 0xAA, g: 0xF8, b: 0xC9 },
			0x3C => Color { r: 0xAC, g: 0xEE, b: 0xF7 },
			0x3D => Color { r: 0xB7, g: 0xB7, b: 0xB7 },
			0x3E => Color { r: 0x00, g: 0x00, b: 0x00 },
			0x3F => Color { r: 0x00, g: 0x00, b: 0x00 },
			_ => panic!("Unknown color!"),
		}
	}
}


#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct Sprite {
	y: u8,
	tile: u8,
	attributes: u8,
	x: u8,
	index: u8,
	low: u8,
	high: u8,
}


impl Sprite {
	fn get_pixel(&self, x: u8) -> u8 {
		let x = if self.attributes & 0x40 == 0 { x } else { 7 - x };

		let high_bit = (self.high >> x) & 1;
		let low_bit = (self.low >> x) & 1;
		if high_bit == 0 && low_bit == 0 {
			return 0;
		}

		((self.attributes & 0x03) << 2) | (high_bit + high_bit + low_bit)
	}
}


pub struct Ppu {
	mapper: Rc<RefCell<dyn Mapper>>,
	v: u16,		// Current VRAM address
	t: u16,		// Temp VRAM address
	x: u8,		// Fine scroll
	w: bool,	// Write toggle (first or second byte)

	pub reg_ppuctrl: u8,
	reg_ppumask: u8,
	reg_ppustatus: u8,
	pub reg_oamaddr: u8,
	reg_ppudata: u8,

	frame: u64,
	pub scan_line: u16,
	pub scan_line_cycle: u16,

	vblank_set: bool,

	gen_latch: u8,
	bg_tile_low_latch: u8,
	bg_tile_low_shift: u16,
	bg_tile_high_latch: u8,
	bg_tile_high_shift: u16,

	at: u8,
	at_low_latch: u8,
	at_high_latch: u8,
	at_low_shift: u8,
	at_high_shift: u8,

	palette_ram: [u8; 32],
	pub oam: Vec<u8>,
	oam_secondary: [Sprite; 8],
	oam_current: [Sprite; 8],
	sprites_found: u8,
	sprite_n: u8,
	sprite_m: u8,

	pub picture: [Color; 256 * 240], // 256 * 240 b
}


#[derive(Copy, Clone, Debug)]
struct SpritePixel {
	color: u8,
	priority: bool,
	index: u8,
}


impl Ppu {
	pub fn new(mapper: Rc<RefCell<dyn Mapper>>) -> Ppu {
		Ppu {
			mapper,
			v: 0,		// Current VRAM address
			t: 0,		// Temp VRAM address
			x: 0,		// Fine scroll
			w: false,	// Write toggle (first or second byte)

			reg_ppuctrl: 0,
			reg_ppumask: 0,
			reg_ppustatus: 0,
			reg_oamaddr: 0,
			reg_ppudata: 0,

			frame: 0,
			scan_line: 0,
			scan_line_cycle: 0,

			vblank_set: false,

			gen_latch: 0,
			bg_tile_low_latch: 0,
			bg_tile_low_shift: 0,
			bg_tile_high_latch: 0,
			bg_tile_high_shift: 0,

			at: 0,
			at_low_latch: 0,
			at_high_latch: 0,
			at_low_shift: 0,
			at_high_shift: 0,

			palette_ram: [0; 32],
			oam: vec![0; 256],
			oam_secondary: [SPRITE_EMPTY; 8],
			oam_current: [SPRITE_EMPTY; 8],
			sprites_found: 0,
			sprite_n: 0,
			sprite_m: 0,

			picture: [Color { r: 0, g: 0, b: 0 }; 256 * 240],
		}
	}


	pub fn read_reg_ubyte(&mut self, address: u16) -> u8 {
		match address & 0x2007 {
			REG_PPUSTATUS => {
				let result = (self.gen_latch & 0x1F) | self.reg_ppustatus;
				self.set_flag(Flag::VerticalBlank, false);
				self.w = false;
				result
			}
			REG_OAMDATA => {
				if self.scan_line < SCAN_LINE_POST_RENDER && self.scan_line_cycle - 1 < 64 && (self.reg_ppumask & 0x18 != 0) {
					0xFF
				}
				else {
					self.oam[self.reg_oamaddr as usize]
				}
			}
			REG_PPUDATA => {
				let result;
				if self.v >= 0x3F00 {
					result = self.read_ubyte(self.v);
					self.reg_ppudata = self.read_ubyte(self.v - 0x1000);
					if self.reg_ppumask & 1 != 0 {
						self.reg_ppudata &= 0x30;
					}
				}
				else {
					result = self.reg_ppudata;
					self.reg_ppudata = self.read_ubyte(self.v);
				}
				self.v += if self.reg_ppuctrl & 0x04 != 0 { 32 } else { 1 };
				result
			}
			_ =>
				self.gen_latch
		}
	}


	pub fn write_reg_ubyte(&mut self, address: u16, value: u8) {
		match address & 0x2007 {
			REG_PPUCTRL => {
				self.set_t_namespace(value);
				self.reg_ppuctrl = value;
			}
			REG_PPUMASK =>
				self.reg_ppumask = value,
			REG_OAMADDR =>
				self.reg_oamaddr = value,
			REG_OAMDATA =>
				if (self.scan_line > SCAN_LINE_POST_RENDER && self.scan_line < SCAN_LINE_PRE_RENDER) || (self.reg_ppumask & 0x18) != 0 {
					self.oam[self.reg_oamaddr as usize] = value;
					self.reg_oamaddr = (Wrapping(self.reg_oamaddr) + Wrapping(1)).0;
				},
			REG_PPUSCROLL => {
				self.w = !self.w;
				if self.w {
					self.t = (self.t & 0x7FE0) | value as u16 >> 3;
					self.x = value & 0x07;
				}
				else {
					self.t = (self.t & 0x1C1F) | ((value as u16 & 0x07) << 12) | ((value as u16 & 0xF8) << 2);
				}
			}
			REG_PPUADDR => {
				self.w = !self.w;
				if self.w {
					self.t = (self.t & 0x00FF) | ((value as u16 & 0x3F) << 8);
				}
				else {
					self.t = (self.t & 0xFF00) | value as u16;
					self.v = self.t;
				}
			}
			REG_PPUDATA => {
				self.write_ubyte(self.v, value);
				self.v += if self.reg_ppuctrl & 0x04 != 0 { 32 } else { 1 };
			}
			_ =>
				println!("Unimplemented PPU register write at local address {:02X}", address),
		}

		self.gen_latch = value;
	}



	fn set_t_namespace(&mut self, value: u8) {
		self.t = (self.t & 0x73FF) | ((value as u16 & 0x03) << 10);
	}


	pub fn emulate(&mut self) -> PpuResult {
		let status = self.reg_ppustatus;
		let rendering_enabled = self.reg_ppumask & 0x18 != 0;
		let scan_line = self.scan_line;

		if scan_line < SCAN_LINE_POST_RENDER {    	// Visible lines
			if rendering_enabled {
				self.handle_bg_pre_fetches();
				self.handle_sprite_pre_fetches();
			}
			self.render_visible_line();
		}
		else if scan_line < SCAN_LINE_PRE_RENDER {	// Post-render line & vertical sync lines
			if scan_line == SCAN_LINE_VBLANK_0 {
				self.handle_vblank_0();
			}
		}
		else {                                      	// Pre-render line
			if self.scan_line_cycle == 1 {
				self.reg_ppustatus = 0x00;				// Reset vertical blank, sprite-0, overflow
				self.oam_current = [SPRITE_EMPTY; 8];
			}
			if rendering_enabled {
				self.handle_bg_pre_fetches();

				if self.scan_line_cycle >= 280 && self.scan_line_cycle <= 304 {
					self.v = (self.v & !0x7BE0) | (self.t & 0x7BE0);	// Reset vertical v bits from t
				}
			}
		}

		PpuResult {
			frame_ready: self.ppu_update_scan_line_cycle(rendering_enabled),
			nmi: !status & self.reg_ppustatus & self.reg_ppuctrl & 0x80 != 0
		}
	}

	fn handle_bg_pre_fetches(&mut self) {
		let cycle = self.scan_line_cycle;
		if cycle == 0 {
			return;
		}

		if cycle <= 256 || cycle >= 321 {
			if (2..=337).contains(&cycle) {
				self.at_low_shift = (self.at_low_shift << 1) | self.at_low_latch;
				self.at_high_shift = (self.at_high_shift << 1) | self.at_high_latch;
				self.bg_tile_high_shift <<= 1;
				self.bg_tile_low_shift <<= 1;
			}

			match (cycle - 1) & 0x07 {
				0 => {
					self.at_low_latch = self.at & 1;
					self.at_high_latch = (self.at >> 1) & 1;
					self.bg_tile_low_shift |= self.bg_tile_low_latch as u16;
					self.bg_tile_high_shift |= self.bg_tile_high_latch as u16;
					let tile = self.read_ubyte(0x2000 | (self.v & 0x0FFF));
					let (low_temp, high_temp) = self.fetch_bg_tile_data(tile);
					if cycle < 337 {
						self.bg_tile_low_latch = low_temp;
						self.bg_tile_high_latch = high_temp;
					}
				}
				3 =>
					if cycle < 337 {
						self.at = self.fetch_attribute_data();
					}
				7 =>
					self.increment_coarse_x(),
				_ => { }
			}
		}

		if cycle == 256 {
			self.increment_y();
		}
		if cycle == 257 {
			self.v = (self.v & !0x041F) | (self.t & 0x041F);
		}
	}

	fn fetch_bg_tile_data(&self, tile: u8) -> (u8, u8) {
		let address = ((self.reg_ppuctrl as u16 & 0x10) << 8) | ((tile as u16) << 4) | (self.v >> 12);
		(
			self.read_ubyte(address),
			self.read_ubyte(address | 0x08)
		)
	}


	fn increment_coarse_x(&mut self) {
		if (self.v & 0x001F) != 0x001F {
			self.v += 1;
		}
		else {
			self.v &= !0x001F;
			self.v ^= 0x0400;
		}
	}

	fn increment_y(&mut self) {
		if (self.v & 0x7000) != 0x7000 {
			self.v += 0x1000;
			return;
		}

		self.v &= !0x7000;
		let mut coarse_y = (self.v & 0x03E0) >> 5;
		if coarse_y == 29 {		// Switch nametable
			coarse_y = 0;
			self.v ^= 0x0800;
		}
		else if coarse_y == 31 {	// Overflow
			coarse_y = 0;
		}
		else {
			coarse_y += 1;
		}

		self.v = (self.v & !0x03E0) | (coarse_y << 5);
	}


	fn fetch_attribute_data(&self) -> u8 {
		let address = 0x23C0 | (self.v & 0x0C00) | ((self.v >> 4) & 0x38) | ((self.v >> 2) & 0x07);
		let mut result = self.read_ubyte(address);

		if self.v & 0x40 != 0 {
			if self.v & 0x02 != 0 {
				result >>= 6;
			}
			else {
				result >>= 4;
			}
		}
		else if self.v & 0x02 != 0 {
			result >>= 2;
		}

		result & 0x03
	}

	fn handle_sprite_pre_fetches(&mut self) {
		if self.scan_line_cycle < 64 {
		}
		else if self.scan_line_cycle == 64 {
			self.oam_secondary = [SPRITE_EMPTY; 8];
			self.sprites_found = 0;
			self.sprite_n = 0;
			self.sprite_m = 0;
		}
		else if self.scan_line_cycle <= 256 {
			if self.scan_line_cycle & 1 == 0 {
				return;
			}

			if self.sprites_found < 8 {
				let sprite_y = self.oam[self.sprite_n as usize * 4 + OAM_SPRITE_Y];
				self.oam_secondary[self.sprites_found as usize].y = sprite_y;

				let sprite_height: u16 = if self.reg_ppuctrl & 0x20 != 0 { 16 } else { 8 };
				if sprite_y as u16 <= self.scan_line && sprite_y as u16 + sprite_height > self.scan_line {
					self.oam_secondary[self.sprites_found as usize].index = self.sprite_n;
					self.oam_secondary[self.sprites_found as usize].tile = self.oam[self.sprite_n as usize * 4 + OAM_SPRITE_TILE];
					self.oam_secondary[self.sprites_found as usize].attributes = self.oam[self.sprite_n as usize * 4 + OAM_SPRITE_ATTRIBUTES];
					self.oam_secondary[self.sprites_found as usize].x = self.oam[self.sprite_n as usize * 4 + OAM_SPRITE_X];
					self.sprites_found += 1;
				}

				self.sprite_n += 1;
				if self.sprite_n == 64 {
					self.sprite_n = 4;
				}
			}
			else if self.sprite_n < 64 {
				let sprite_y = self.oam[self.sprite_n as usize * 4 + self.sprite_m as usize] as u16;
				if sprite_y < self.scan_line && sprite_y + 8 >= self.scan_line {
					self.set_flag(Flag::SpriteOverflow, true);
					self.sprite_m = (self.sprite_m + 1) % 4;
					if self.sprite_m == 0 {
						self.sprite_n += 1;
					}
				}
				else {
					self.sprite_m = (self.sprite_m + 1) % 4;
					self.sprite_n += 1;
				}
			}
		}
		else if self.scan_line_cycle <= 320 {
			if self.scan_line_cycle & 0x07 == 0x03 {
				let index = self.scan_line_cycle as usize >> 3 & 0x07;
				let sprite = &self.oam_secondary[index];
				let mut y = (Wrapping(self.scan_line) - Wrapping(sprite.y as u16)).0 as u8;

				let mut address: u16;
				let tall_sprites = self.reg_ppuctrl & 0x20 != 0;
				if sprite.attributes & 0x80 != 0 {
					y = (Wrapping(if tall_sprites { 15 } else { 7 }) - Wrapping(y)).0;
				}

				let mut tile = sprite.tile;
				if tall_sprites {
					address = (tile as u16 & 1) << 12;
					if y >= 8 {
						tile |= 1;
						y -= 8;
					}
					else {
						tile &= 0xFE;
					}
				}
				else {
					address = (self.reg_ppuctrl as u16 & 0x08) << 9;
				}

				address |= ((tile as u16) << 4) | y as u16;

				self.oam_secondary[index].low = self.read_ubyte(address);
				self.oam_secondary[index].high = self.read_ubyte(address | 0x8);
			}
		}
		else if self.scan_line_cycle == 321 {
			self.oam_current = self.oam_secondary;
		}
	}

	fn render_visible_line(&mut self) {
		let cycle = self.scan_line_cycle;

		if (1..=256).contains(&cycle) {
			let sprite_pixel = self.get_sprite_pixel();
			let bg_color = self.get_bg_color();

			if sprite_pixel.index == 0 && bg_color != 0 && sprite_pixel.color != 0 {
				self.set_flag(Flag::SpriteZeroHit, true);
			}

			let mut palette: u16 = 0x3F00;
			if bg_color != 0 && (sprite_pixel.priority || sprite_pixel.color == 0) {
				palette |= (((self.at_high_shift as u16 >> (7 - self.x)) & 1) << 3) | (((self.at_low_shift as u16 >> (7 - self.x)) & 1) << 2) | bg_color as u16;
			}
			else {
				palette |= 0x10 | sprite_pixel.color as u16;
			}

			let mut color = self.read_ubyte(palette);
			if self.reg_ppumask & 1 != 0 {
				color &= 0x30;
			}

			self.picture[((self.scan_line << 8) | (cycle - 1)) as usize] = Color::new(color);
		}
	}


	fn get_bg_color(&self) -> u8 {
		if (self.reg_ppumask & 0x08) == 0 || (self.scan_line_cycle <= 8 && (self.reg_ppumask & 0x04) == 0) {
			0
		}
		else {
			let x = 0x01u16 << (15 - self.x);
			(if self.bg_tile_high_shift & x != 0 { 2 } else { 0 }) | (if self.bg_tile_low_shift & x != 0 { 1 } else { 0 })
		}
	}



	fn get_sprite_pixel(&mut self) -> SpritePixel {
		let mut result = SpritePixel { color: 0x00, priority: true, index: 0xFF };
		let cycle = self.scan_line_cycle;

		if (cycle > 8 || self.reg_ppumask & 0x04 != 0) && (self.reg_ppumask & 0x10 != 0) {
			for i in 0..8 {
				let sprite = &mut self.oam_current[i];
				let x = sprite.x as u16;
				if x < cycle && x + 8 >= cycle {
					let sprite_x = (8 + x - cycle) as u8;
					let color = sprite.get_pixel(sprite_x);
					if color == 0 {
						continue;
					}

					result.color = color;
					result.priority = sprite.attributes & 0x20 != 0;
					result.index = sprite.index;
					break;
				}
			}
		}

		result
	}


	fn handle_vblank_0(&mut self) {
		if self.scan_line_cycle == 0 {
			self.vblank_set = true;
		}
		else if self.scan_line_cycle == 1 && self.vblank_set {
			self.set_flag(Flag::VerticalBlank, true);
		}
	}

	fn ppu_update_scan_line_cycle(&mut self, rendering_enabled: bool) -> bool {
		self.scan_line_cycle += 1;
		if self.scan_line_cycle < 341 {
			return false;
		}

		self.scan_line_cycle = 0;
		self.scan_line += 1;
		if self.scan_line > SCAN_LINE_PRE_RENDER {
			self.scan_line = 0;
			self.frame += 1;
			if rendering_enabled && (self.frame & 1 != 0) {
				self.scan_line_cycle += 1;
			}
			true
		}
		else {
			false
		}
	}


	fn set_flag(&mut self, flag: Flag, value: bool) {
		if value {
			self.reg_ppustatus |= (1 << flag as u8) as u8
		}
		else {
			self.reg_ppustatus &= !((1 << flag as u8) as u8)
		}
	}

	pub fn peek_ubyte(&self, address: u16) -> u8 {
		if address < 0x3F00 {
			self.mapper.borrow().ppu_peek_ubyte(address)
		}
		else if address < 0x4000 {	// Palettes
			let index = if address & 0x13 == 0x10 { (address & 0b00011111) ^ 0x10 } else { address & 0b00011111 };
			self.palette_ram[index as usize]
		}
		else {
			println!("PPU: invalid read at address ${:X}", address);
			0x00
		}
	}


	pub fn read_ubyte(&self, address: u16) -> u8 {
		if address < 0x3F00 {
			self.mapper.borrow_mut().ppu_read_ubyte(address)
		}
		else if address < 0x4000 {	// Palettes
			let index = if address & 0x13 == 0x10 { (address & 0b00011111) ^ 0x10 } else { address & 0b00011111 };
			self.palette_ram[index as usize]
		}
		else {
			println!("PPU: invalid read at address ${:X}", address);
			0x00
		}
	}

	fn write_ubyte(&mut self, address: u16, value: u8) {
		if address < 0x3F00 {
			self.mapper.borrow_mut().ppu_write_ubyte(address, value);
		}
		else if address < 0x4000 {	// Palettes
			let index = if address & 0x13 == 0x10 { (address & 0b00011111) ^ 0x10 } else { address & 0b00011111 };
			self.palette_ram[index as usize] = value & 0x3F;
		}
		else {
			println!("PPU: invalid write at address ${:04X}, value ${:02X}", address, value);
		}
	}
}


impl State for Ppu {
	fn save(&self, file: &mut File) {
		state::serialize(&self.v, file);
		state::serialize(&self.t, file);
		state::serialize(&self.x, file);
		state::serialize(&self.w, file);

		state::serialize(&self.reg_ppuctrl, file);
		state::serialize(&self.reg_ppumask, file);
		state::serialize(&self.reg_ppustatus, file);
		state::serialize(&self.reg_ppudata, file);

		state::serialize(&self.frame, file);
		state::serialize(&self.scan_line, file);
		state::serialize(&self.scan_line_cycle, file);

		state::serialize(&self.vblank_set, file);
		state::serialize(&self.palette_ram, file);
		state::serialize(&self.oam, file);
	}

	fn load(&mut self, file: &mut File) {
		self.v = state::deserialize(file);
		self.t = state::deserialize(file);
		self.x = state::deserialize(file);
		self.w = state::deserialize(file);

		self.reg_ppuctrl = state::deserialize(file);
		self.reg_ppumask = state::deserialize(file);
		self.reg_ppustatus = state::deserialize(file);
		self.reg_ppudata = state::deserialize(file);

		self.frame = state::deserialize(file);
		self.scan_line = state::deserialize(file);
		self.scan_line_cycle = state::deserialize(file);

		self.vblank_set = state::deserialize(file);
		self.palette_ram = state::deserialize(file);
		self.oam = state::deserialize(file);
	}
}
