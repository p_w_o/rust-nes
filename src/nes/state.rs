use std::fs::File;
use bincode::Result;
use serde::de::DeserializeOwned;
use serde::Serialize;


pub trait State {

	fn save(&self, file: &mut File);

	fn load(&mut self, file: &mut File);

}

pub fn serialize<T: Serialize>(value: &T, file: &mut File) {
	bincode::serialize_into(file, value).unwrap();
}

pub fn deserialize<T: DeserializeOwned>(file: &mut File) -> T {
	bincode::deserialize_from(file).unwrap()
}


pub fn try_deserialize<T: DeserializeOwned>(file: &mut File) -> Result<T> {
	bincode::deserialize_from(file)
}
