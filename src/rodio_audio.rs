use rodio::buffer::SamplesBuffer;
use rodio::{OutputStream, Sink};

use crate::ui::audio::Audio;


pub struct RodioAudio {
	_stream: OutputStream,
	sink: Sink,
}


impl RodioAudio {
	pub fn new() -> RodioAudio {
		let (_stream, stream_handle) = OutputStream::try_default().unwrap();
		let sink = Sink::try_new(&stream_handle).unwrap();
		sink.set_speed(1.0);
		
		RodioAudio {
			_stream,
			sink,
		}
	}
}


impl Audio for RodioAudio {

	fn play(&mut self, samples: &[f32]) {
		let buffer = SamplesBuffer::new(1, 1768000, samples);
		self.sink.append(buffer);
	}

	fn set_fps(&mut self, fps: f64) {
        self.sink.set_speed(fps as f32 / 60.0);
    }

}
