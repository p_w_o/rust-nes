use sdl2::audio::{AudioCVT, AudioFormat, AudioQueue, AudioSpecDesired};
use sdl2::Sdl;
use std::convert::TryInto;

use crate::ui::audio::Audio;


pub struct SdlAudio {
	queue: AudioQueue<f32>,
	converter: AudioCVT,
}


impl SdlAudio {
	pub fn new(sdl_context: Sdl) -> SdlAudio {
		let audio_subsystem = sdl_context.audio().unwrap();

		let desired_spec = AudioSpecDesired {
		    freq: Some(44100),
		    channels: Some(1),  // mono
		    samples: None       // default sample size
		};

		let converter = AudioCVT::new(
			AudioFormat::F32LSB,
			1,
			1768000,
			AudioFormat::F32LSB,
			1,
			44100,
		).unwrap();

		let queue = audio_subsystem.open_queue(None, &desired_spec).unwrap();
		queue.resume();

		SdlAudio {
			queue,
			converter,
		}
	}
}


impl Audio for SdlAudio {

	fn play(&mut self, samples: &[f32]) {
		let converted = to_f32(self.converter.convert(to_u8(samples)));
		if self.queue.size() as usize > 4410 {
			self.queue.clear();
		}
		self.queue.queue_audio(&converted).unwrap();
	}

	fn set_fps(&mut self, _fps: f64) {
		// Not supported
	}

}

fn to_u8(samples: &[f32]) -> Vec<u8> {
	let mut buffer = Vec::with_capacity(samples.len() * 4);

	for f in samples.iter() {
		for b in f.to_le_bytes().iter() {
			buffer.push(*b);
		}
	}

	buffer
}


fn to_f32(samples: Vec<u8>) -> Vec<f32> {
	let count = samples.len() / 4;
	let mut buffer = Vec::with_capacity(count);

	for i in 0..count {
		buffer.push(f32::from_le_bytes(samples[i * 4..(i + 1) * 4].try_into().unwrap()));
	}

	buffer
}