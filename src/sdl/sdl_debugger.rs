use sdl2::Sdl;

use crate::nes::ppu::Color;
use crate::ui::debugger::{DebugUi, BgDebugger, SpriteDebugger};

use std::sync::mpsc::{self, Receiver};

use crate::nes::debug::Debug;

use super::sdl_window::{SdlWindow, Layer};


pub struct SdlBgDebugger {
	rx: Option<Receiver<Vec<Color>>>,
	window: SdlWindow,
}


impl DebugUi for SdlBgDebugger {
	fn debugger(&mut self) -> Box<dyn Debug> {
		let (tx, rx) = mpsc::channel();
		self.rx = Some(rx);
		Box::new(BgDebugger::new(tx))
	}

	fn present(&mut self) {
		if let Some(rx) = &self.rx {
			if let Ok(data) = rx.try_recv() {
				self.window.draw(vec!(&Layer::main(256 * 2, 240 * 2, data)));
			}
		}
	}
}

impl SdlBgDebugger {
	pub fn new(sdl_context: Sdl) -> SdlBgDebugger {
		let video_subsystem = sdl_context.video().unwrap();
		SdlBgDebugger {
			rx: None,
			window: SdlWindow::new(video_subsystem, "rust-nes - BG debugger", 512, 419),
		}
	}
}


pub struct SdlSpriteDebugger {
	rx: Option<Receiver<Vec<Color>>>,
	window: SdlWindow,
}


impl DebugUi for SdlSpriteDebugger {
	fn debugger(&mut self) -> Box<dyn Debug> {
		let (tx, rx) = mpsc::channel();
		self.rx = Some(rx);
		Box::new(SpriteDebugger::new(tx))
	}

	fn present(&mut self) {
		if let Some(rx) = &self.rx {
			if let Ok(data) = rx.try_recv() {
				self.window.draw(vec!(&Layer::main(8 * 8, 8 * 17, data)));
			}
		}
	}
}


impl SdlSpriteDebugger {
	pub fn new(sdl_context: Sdl) -> SdlSpriteDebugger {
		let video_subsystem = sdl_context.video().unwrap();
		SdlSpriteDebugger {
			rx: None,
			window: SdlWindow::new(video_subsystem, "rust-nes - Sprite debugger", 256, 475),
		}
	}
}
