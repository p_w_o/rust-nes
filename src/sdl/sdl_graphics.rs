use std::{time::{Instant, Duration}, ops::Add};

use sdl2::Sdl;

use crate::nes::ppu::Color;
use crate::ui::graphics::Graphics;

use super::sdl_window::{SdlWindow, Layer, ScaleMode, Position};

const COLOR_TEXT: Color = Color { r: 0xFF, g: 0xFF, b: 0xFF };


pub struct SdlGraphics {
	window: SdlWindow,
	fps: f64,
	fps_show: bool,
	slot_layer: Option<TempLayer>,
}


impl SdlGraphics {
	pub fn new(sdl_context: Sdl) -> SdlGraphics {
	    let video_subsystem = sdl_context.video().unwrap();
		let mut result = SdlGraphics {
			window: SdlWindow::new(video_subsystem, "rust-nes", 768, 629),
			fps: 0.0,
			fps_show: false,
			slot_layer: None,
		};
		result.set_fullscreen(false);
		result
	}
}


impl Graphics for SdlGraphics {

	fn draw(&mut self, picture: &[Color; 256 * 240]) {
		let main_layer = Layer::main(256, 240, picture.to_vec());
		let fps_layer;
		let mut layers = vec!(&main_layer);
		if self.fps_show {
			fps_layer = Layer::float(self.fps, COLOR_TEXT, super::sdl_window::Position::End);
			layers.push(&fps_layer);
		}

		if let Some(slot_layer) = &self.slot_layer {
			if slot_layer.show_until > Instant::now() {
				layers.push(&slot_layer.layer);
			}
		}

		self.window.draw(layers);
	}


	fn set_fullscreen(&mut self, fullscreen: bool) {
		self.window.set_fullscreen(fullscreen);
	}

	fn set_fps(&mut self, fps: f64) {
		let title = format!("rust-nes - {:0.1} FPS", fps);
		self.fps = fps;
		self.window.set_title(&title);
	}

	fn set_show_fps(&mut self, show: bool) {
		self.fps_show = show;
	}

	fn show_save_slot(&mut self, slot: u32, picture: Option<Vec<Color>>) {
		let mut layer = Layer::integer(slot, COLOR_TEXT, super::sdl_window::Position::Start);
		if let Some(picture) = picture {
			let mut picture_layer: Layer = Layer::picture(256, 240, picture, ScaleMode::None, Position::Start, Position::Start);
			picture_layer.overlay(&layer);
			layer = picture_layer;
		}

		let temp_layer = TempLayer {
			layer,
			show_until: Instant::now().add(Duration::from_secs(1)),
		};

		self.slot_layer = Some(temp_layer);
	}

}


struct TempLayer {
	layer: Layer,
	show_until: Instant,
}