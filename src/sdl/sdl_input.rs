use sdl2::{ EventPump, GameControllerSubsystem, Sdl };
use sdl2::controller::{ Axis, Button, GameController };
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use crate::ui::input::{NesButton, InputState, Input, Action};


const PAD_DEAD_ZONE: i16 = 10240;


pub struct SdlInput {
	event_pump: EventPump,
	controllers: SdlControllers,
	kb_state: u8,
	fullscreen: bool,
}


struct SdlControllers {
	controller_subsystem: GameControllerSubsystem,
	controllers: [Option<GameController>; 2],
}


impl SdlInput {
	pub fn new(sdl_context: Sdl) -> SdlInput {
		let controller_subsystem = sdl_context.game_controller().unwrap();

		let mut controllers = SdlControllers {
			controller_subsystem,
			controllers: [ Option::None, Option::None ],
		};
		controllers.init();

		SdlInput {
			event_pump: sdl_context.event_pump().unwrap(),
			controllers,
			kb_state: 0,
			fullscreen: false,
		}
	}

}


impl SdlControllers {

	fn init(&mut self) {
		let mut controller_0 = self.controllers[0].take().filter(GameController::attached);
		let mut controller_1 = self.controllers[1].take().filter(GameController::attached);
		if controller_0.is_none() || controller_1.is_none() {
			let count = self.controller_subsystem.num_joysticks().unwrap();
			for i in 0..count {
				if !self.controller_subsystem.is_game_controller(i) {
					continue;
				}

				if i == get_id(&controller_0) || i == get_id(&controller_1) {
					continue;
				}

				let c = self.controller_subsystem.open(i).unwrap();
				if controller_0.is_none() {
					println!("Player 1: {}", c.name());
					controller_0 = Some(c);
				}
				else {
					println!("Player 2: {}", c.name());
					controller_1 = Some(c);
					break;
				}
			}
		}

		self.controllers[0] = controller_0;
		self.controllers[1] = controller_1;
	}


	fn query_state(&mut self) -> (u8, u8) {
		(
			query_controller_state(&self.controllers[0]),
			query_controller_state(&self.controllers[1])
		)
	}

}

fn query_controller_state(controller: &Option<GameController>) -> u8 {
	let mut result = 0;

	if let Some(c) = controller {
		if c.button(Button::A) || c.button(Button::Y) {
			result |= NesButton::A as u8;
		}
		if c.button(Button::B) || c.button(Button::X) {
			result |= NesButton::B as u8;
		}
		if c.button(Button::Back) {
			result |= NesButton::Select as u8;
		}
		if c.button(Button::Start) {
			result |= NesButton::Start as u8;
		}

		let axis_y = c.axis(Axis::LeftY);
		if c.button(Button::DPadUp) || axis_y < -PAD_DEAD_ZONE {
			result |= NesButton::Up as u8;
		}
		if c.button(Button::DPadDown) || axis_y > PAD_DEAD_ZONE {
			result |= NesButton::Down as u8;
		}

		let axis_x = c.axis(Axis::LeftX);
		if c.button(Button::DPadLeft) || axis_x < -PAD_DEAD_ZONE {
			result |= NesButton::Left as u8;
		}
		if c.button(Button::DPadRight) || axis_x > PAD_DEAD_ZONE {
			result |= NesButton::Right as u8;
		}
	}

	result
}


fn get_id(controller: &Option<GameController>) -> u32 {
	match controller {
		Some(c) => c.instance_id(),
		_ => u32::MAX,
	}
}


impl Input for SdlInput {
	fn handle_input(&mut self) -> InputState {
		let mut actions = Vec::new();
		let mut kb_state = self.kb_state;

		for event in self.event_pump.poll_iter() {
			match event {
				Event::KeyDown { keycode: Some(Keycode::Escape), .. } =>
					if self.fullscreen {
						self.fullscreen = false;
						actions.push(Action::Windowed);
					}
					else {
						actions.push(Action::Quit);
					},
				Event::KeyDown { keycode: Some(Keycode::Kp0), .. } |
				Event::KeyDown { keycode: Some(Keycode::Num0), .. } =>
					actions.push(Action::FpsReset),
				Event::KeyDown { keycode: Some(Keycode::KpPlus), .. } |
				Event::KeyDown { keycode: Some(Keycode::Plus), .. } =>
					actions.push(Action::FpsIncrease),
				Event::KeyDown { keycode: Some(Keycode::KpMinus), .. } |
				Event::KeyDown { keycode: Some(Keycode::Minus), .. } =>
					actions.push(Action::FpsDecrease),
				Event::KeyDown { keycode: Some(Keycode::Delete), .. } =>
					actions.push(Action::FpsLimitToggle),
				Event::ControllerButtonDown { button: Button::RightShoulder, .. } |
				Event::KeyDown { keycode: Some(Keycode::F1), .. } =>
					actions.push(Action::Save),
				Event::ControllerButtonUp { button: Button::LeftShoulder, .. } |
				Event::KeyDown { keycode: Some(Keycode::F3), .. } =>
					actions.push(Action::Load),
				Event::ControllerButtonUp { button: Button::RightStick, .. } |
				Event::KeyDown { keycode: Some(Keycode::F2), .. } =>
					actions.push(Action::SaveSlotNext),
				Event::ControllerButtonUp { button: Button::LeftStick, .. } =>
					actions.push(Action::SaveSlotPrev),
				Event::ControllerDeviceAdded { .. }| Event::ControllerDeviceRemoved { .. } =>
					self.controllers.init(),
				Event::KeyDown { keycode: Some(Keycode::F11), .. } => {
					self.fullscreen = !self.fullscreen;
					actions.push(if self.fullscreen { Action::Fullscreen } else { Action::Windowed });
				},
				Event::KeyDown { keycode: Some(Keycode::F12), .. } =>
					actions.push(Action::FpsShowToggle),
				Event::KeyDown { keycode: Some(code), ..} =>
					kb_state |= to_pad_mask(code),
				Event::KeyUp { keycode: Some(code), ..} =>
					kb_state &= !to_pad_mask(code),
				Event::Window { .. } =>
					actions.push(if self.fullscreen { Action::Fullscreen } else { Action::Windowed }),
				Event::Quit {..} =>
					actions.push(Action::Quit),
				_ => {}
			}
		}

		self.kb_state = kb_state;

		let (s0, s1) = self.controllers.query_state();

		InputState {
			actions,
			pads: [s0 | kb_state, s1],
		}
	}
}


fn to_pad_mask(key_code: Keycode) -> u8 {
	match key_code {
		Keycode::LAlt |
		Keycode::Space |
		Keycode::X =>
			NesButton::A as u8,
		Keycode::LCtrl |
		Keycode::Z =>
			NesButton::B as u8,
		Keycode::LShift =>
			NesButton::Select as u8,
		Keycode::Return =>
			NesButton::Start as u8,
		Keycode::Up =>
			NesButton::Up as u8,
		Keycode::Down =>
			NesButton::Down as u8,
		Keycode::Left =>
			NesButton::Left as u8,
		Keycode::Right =>
			NesButton::Right as u8,
		_ => 0,
	}
}
