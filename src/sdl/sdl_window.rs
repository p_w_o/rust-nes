use sdl2::VideoSubsystem;
use sdl2::pixels::PixelFormatEnum;
use sdl2::rect::Rect;
use sdl2::render::{WindowCanvas, TextureCreator};
use sdl2::video::{ WindowContext, FullscreenType};

use crate::nes::ppu::Color;

const COLOR_BLACK: Color = Color { r: 0x00, g: 0x00, b: 0x00 };

const CHAR_0: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_1: [bool; 16 * 4] = [
		false, false, true,  true,
		false, true,  true,  true,
		true,  true,  true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
		false, false, true,  true,
];

const CHAR_2: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, true,  true,  true,
		false, false, false, false, true,  true,  false, false,
		false, false, false, true,  true,  false, false, false,
		false, false, true,  true,  false, false, false, false,
		false, true,  true,  false, false, false, false, false,
		true,  true,  true,  false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  true,  true,  true,  true,  true,
		true,  true,  true,  true,  true,  true,  true,  true,
];

const CHAR_3: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, true,  true,  true,
		false, false, false, true,  true,  true,  true,  false,
		false, false, false, true,  true,  true,  true,  false,
		false, false, false, false, false, true,  true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_4: [bool; 16 * 8] = [
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, false, true,  true,
		false, true,  true,  true,  true,  true,  true,  true,
		false, false, true,  true,  true,  true,  true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
];

const CHAR_5: [bool; 16 * 8] = [
		true,  true,  true,  true,  true,  true,  true,  true,
		true,  true,  true,  true,  true,  true,  true,  true,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  true,  false, false, false, false, false,
		false, true,  true,  true,  true,  true,  false, false,
		false, false, true,  true,  true,  true,  true,  false,
		false, false, false, false, false, true,  true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_6: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  true,  true,  true,  true,  false, false,
		true,  true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_7: [bool; 16 * 8] = [
		true,  true,  true,  true,  true,  true,  true,  true,
		true,  true,  true,  true,  true,  true,  true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, true,  true,  true,
		false, false, true,  true,  true,  true,  true,  false,
		false, true,  true,  true,  true,  true,  false, false,
		true,  true,  true,  false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
		true,  true,  false, false, false, false, false, false,
];

const CHAR_8: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_9: [bool; 16 * 8] = [
		false, false, true,  true,  true,  true,  false, false,
		false, true,  true,  true,  true,  true,  true,  false,
		true,  true,  true,  false, false, true,  true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  true,
		false, false, false, false, false, true,  true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		false, false, false, false, false, false, true,  true,
		true,  true,  true,  false, false, true,  true,  true,
		false, true,  true,  true,  true,  true,  true,  false,
		false, false, true,  true,  true,  true,  false, false,
];

const CHAR_DOT: [bool; 16 * 4] = [
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, false, false, false,
		false, true,  true,  false,
		true,  true,  true,  true,
		true,  true,  true,  true,
		false, true,  true,  false,
];

const CHAR_SPACE: [bool; 16 * 2] = [
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
		false, false,
];


pub struct SdlWindow {
	canvas: WindowCanvas,
	texture_creator: TextureCreator<WindowContext>,
}


impl SdlWindow {
	pub fn new(
			video_subsystem: VideoSubsystem,
			window_title: &str,
			window_width: u32,
			window_height: u32) -> SdlWindow {

		let window = video_subsystem.window(window_title, window_width, window_height)
				.position_centered()
				.resizable()
				.build()
				.unwrap();

		let canvas = window.into_canvas()
				.accelerated()
				.build()
				.unwrap();

		let texture_creator = canvas.texture_creator();
		SdlWindow {
			canvas,
			texture_creator,
		}
	}

	pub fn set_fullscreen(&mut self, fullscreen: bool) {
		self.canvas.window_mut().set_fullscreen(if fullscreen { FullscreenType::Desktop } else { FullscreenType::Off }).unwrap();
	}

	pub fn set_title(&mut self, title: &str) {
		self.canvas.window_mut().set_title(&title).expect("Could not set window title");
	}
}


impl SdlWindow {

	pub fn draw(&mut self, layers: Vec<&Layer>) {
		self.canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
		self.canvas.clear();

		for layer in layers {
			self.draw_layer(&layer);
		}

		self.canvas.present();
	}


	fn draw_layer(&mut self, layer: &Layer) {
		let mut texture = self.texture_creator
				.create_texture_target(PixelFormatEnum::ARGB8888, layer.width as u32, layer.height as u32)
				.expect("Could not create a texture");

		let size = layer.width * layer.height;
		let mut cache: Vec<u8> = Vec::with_capacity(size * 4);
		for i in 0 .. size {
			cache.push(layer.picture[i].b);
			cache.push(layer.picture[i].g);
			cache.push(layer.picture[i].r);
			cache.push(0);
		}
		texture.update(None, &cache, layer.width * 4).expect("Could not update texture");
		let dst = layer.calculate_target(self.canvas.viewport());

		self.canvas.copy(&texture, None, dst).expect("Could not copy texture to screen");
	}

}


pub enum ScaleMode {
	None,
	Fit,
}

pub enum Position {
	Start,
	Center,
	End,
}


pub struct Layer {
	width: usize,
	height: usize,
	pixel_ratio: f32,
	picture: Vec<Color>,
	scale_mode: ScaleMode,
	position_vertical: Position,
	position_horizontal: Position,
}


impl Layer {

	pub fn main(width: usize, height: usize, picture: Vec<Color>) -> Layer {
		Self::picture(width, height, picture, ScaleMode::Fit, Position::Center, Position::Center)
	}

	pub fn picture(width: usize, height: usize, picture: Vec<Color>, scale_mode: ScaleMode, position_vertical: Position, position_horizontal: Position) -> Layer {
		assert!(width * height == picture.len(), "Got invalid size picture");
		Layer {
			width,
			height,
			pixel_ratio: 8.0 / 7.0,
			picture,
			scale_mode,
			position_vertical,
			position_horizontal,
		}
	}

	pub fn integer(value: u32, color: Color, position_horizontal: Position) -> Layer {
		Self::text(&format!("{:0}", value), color, position_horizontal)
	}

	pub fn float(value: f64, color: Color, position_horizontal: Position) -> Layer {
		Self::text(&format!("{:0.1}", value), color, position_horizontal)
	}

	fn text(value: &str, color: Color, position_horizontal: Position) -> Layer {
		let height = 16;
		let mut picture = Vec::with_capacity(value.len() * height);

		let mut glyphs = Vec::new();

		for c in value.chars() {
			if !glyphs.is_empty() {
				glyphs.push(CHAR_SPACE.as_slice());
			}

			match c {
				'0' => glyphs.push(CHAR_0.as_slice()),
				'1' => glyphs.push(CHAR_1.as_slice()),
				'2' => glyphs.push(CHAR_2.as_slice()),
				'3' => glyphs.push(CHAR_3.as_slice()),
				'4' => glyphs.push(CHAR_4.as_slice()),
				'5' => glyphs.push(CHAR_5.as_slice()),
				'6' => glyphs.push(CHAR_6.as_slice()),
				'7' => glyphs.push(CHAR_7.as_slice()),
				'8' => glyphs.push(CHAR_8.as_slice()),
				'9' => glyphs.push(CHAR_9.as_slice()),
				'.' => glyphs.push(CHAR_DOT.as_slice()),
				_ => {},
			}
		}

		draw(&mut picture, glyphs, color);

		Layer {
			width: picture.len() / height,
			height,
			pixel_ratio: 1.0,
			picture,
			scale_mode: ScaleMode::None,
			position_vertical: Position::Start,
			position_horizontal,
		}
	}


	fn calculate_target(&self, viewport: Rect) -> Rect {
		let mut result = match self.scale_mode {
			ScaleMode::None => Rect::new(0, 0, self.width as u32, self.height as u32),
			ScaleMode::Fit => {
				let viewport_ratio = viewport.width() as f32 / viewport.height() as f32;
				let target_ratio = self.width as f32 / self.height as f32 * self.pixel_ratio;
				if viewport_ratio > target_ratio {
					let width = (viewport.height() as f32 * target_ratio) as u32;
					Rect::new(0, 0, width, viewport.height())
				}
				else {
					let height = (viewport.width() as f32 / target_ratio) as u32;
					Rect::new(0, 0, viewport.width(), height)
				}
			}
		};

		result.set_y(calculate_offset(&self.position_vertical, viewport.height(), result.height()));
		result.set_x(calculate_offset(&self.position_horizontal, viewport.width(), result.width()));
		result
	}

	pub fn overlay(&mut self, other: &Layer) {
		for y in 0 .. std::cmp::min(self.height, other.height) {
			for x in 0 .. std::cmp::min(self.width, other.width) {
				self.picture[y * self.width + x] = other.picture[y * other.width + x];
			}
		}
	}

}


fn calculate_offset(position: &Position, viewport_length: u32, target_length: u32) -> i32 {
	match position {
		Position::Start => 0,
		Position::Center => (viewport_length as i32 - target_length as i32) / 2,
		Position::End => viewport_length as i32 - target_length as i32,
	}
}


fn draw(target: &mut Vec<Color>, glyphs: Vec<&[bool]>, color: Color) {
	for row in 0..16 {
		for glyph in &glyphs {
			let len = glyph.len() / 16;
			for value in &glyph[row * len .. (row + 1) * len] {
				target.push(if *value { color } else { COLOR_BLACK });
			}
		}
	}
}