
pub trait Audio {
	fn play(&mut self, samples: &[f32]);

	fn set_fps(&mut self, fps: f64);
}
