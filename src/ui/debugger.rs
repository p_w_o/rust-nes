use crate::nes::ppu::{Color, Ppu};

use std::sync::mpsc::Sender;

use crate::nes::emulator::Emulator;
use crate::nes::debug::Debug;


pub trait DebugUi {
	fn debugger(&mut self) -> Box<dyn Debug>;

	fn present(&mut self);
}


pub struct CpuDump {
}

impl Debug for CpuDump {

	fn debug(&mut self, emulator: &mut Emulator) -> bool {
		if emulator.cpu_skip == 1 && emulator.cycle % 3 == 0 && emulator.cpu().dma_count == 0 {
			emulator.cpu().print_state();
		}
		false
	}

}

impl CpuDump {
	pub fn new() -> CpuDump {
		CpuDump { }
	}
}


const NAMESPACES: [u16; 4] = [0x2000, 0x2400, 0x2800, 0x2C00];

pub struct BgDebugger {
	tx: Sender<Vec<Color>>,
}


impl Debug for BgDebugger {

	fn debug(&mut self, emulator: &mut Emulator) -> bool {
		let ppu = emulator.ppu();
		if ppu.scan_line != 261 || ppu.scan_line_cycle != 340 {
			return false;
		}

		let mut data = vec!(Color::new(0); 4 * 256 * 240);

		for ns in NAMESPACES.iter() {
			self.handle_nametable(ppu, *ns, &mut data);
		}

		self.tx.send(data).unwrap();
		false
	}

}


impl BgDebugger {
	pub fn new(tx: Sender<Vec<Color>>) -> BgDebugger {
		BgDebugger {
			tx,
		}
	}

	fn handle_nametable(&mut self, ppu: &Ppu, base_address: u16, data: &mut [Color]) {
		let tile_base_addr = (ppu.reg_ppuctrl as u16 & 0x10) << 8;
		let mut base_index =
				if base_address & 0x800 == 0 { 0 } else { 240 * 256 * 2 } +
				if base_address & 0x400 == 0 { 0 } else { 256 };

		for y in 0..30 {
			for x in 0..32 {
				let mut index = base_index + x as usize * 8;
				let tile = ppu.read_ubyte(base_address | (y * 32 + x));
				let attributes = ppu.read_ubyte(base_address | 0x03C0 | ((y / 4) * 8 + x / 4));
				let shift = (y / 2) * 4 + (x & 0b10);
				let attribute = (attributes >> shift) & 0b11;

				for tile_y in 0..8 {
					let address = tile_base_addr | ((tile as u16) << 4) | tile_y;
					let low = ppu.read_ubyte(address);
					let high = ppu.read_ubyte(address | 0x08);

					for tile_x in 0..8 {
						let mask = 0b1000_0000 >> tile_x;
						let color = (if high & mask != 0 { 2 } else { 0 }) | (if low & mask != 0 { 1 } else { 0 });
						let address = if color > 0 { 0x3F00 | (attribute as u16) << 2 | color as u16 } else { 0x3F10 };
						data[index + tile_x] = Color::new(ppu.read_ubyte(address));
					}
					index += 512;
				}
			}
			base_index += 512 * 8;
		}
	}

}


pub struct SpriteDebugger {
	tx: Sender<Vec<Color>>,
}

impl SpriteDebugger {

	pub fn new(tx: Sender<Vec<Color>>) -> SpriteDebugger {
		SpriteDebugger {
			tx,
		}
	}

}

impl Debug for SpriteDebugger {

	fn debug(&mut self, emulator: &mut Emulator) -> bool {
		let ppu = emulator.ppu();
		if ppu.scan_line != 261 || ppu.scan_line_cycle != 340 {
			return false;
		}

		let tall_sprites = ppu.reg_ppuctrl & 0x20 != 0;
		let sprite_height: u16 = if tall_sprites { 16 } else { 8 };

		let mut data = vec!(Color::new(0); 8 * 8 * 8 * 17);

		for sprite_index in 0 .. 64 {
			let sprite_data = &ppu.oam[sprite_index * 4 .. (sprite_index + 1) * 4];
			let tile = sprite_data[1];
			let attributes = sprite_data[2];

			let mut i = (sprite_index / 8) * (8 * 8 * (sprite_height as usize + 1)) + (sprite_index % 8) * 8;

			let base_address;
			if tall_sprites {
				base_address = ((tile as u16 & 1) << 12) | ((tile as u16 & 0xFE) << 4); 
			}
			else {
				base_address = (ppu.reg_ppuctrl as u16 & 0x08) << 9 | ((tile as u16) << 4);
			}

			for tile_y in 0..sprite_height {
				let address = base_address + tile_y + if tile_y >= 8 { 0x08} else { 0x00 };
				let low = ppu.peek_ubyte(address);
				let high = ppu.peek_ubyte(address | 0x8);

				for tile_x in 0..8 {
					let mask = 0b1000_0000 >> tile_x;
					let color = (if high & mask != 0 { 2 } else { 0 }) | (if low & mask != 0 { 1 } else { 0 });
					let address = if color > 0 { 0x3F10 | (attributes as u16 & 0b11) << 2 | color } else { 0x3F10 };

					data[i + tile_x] = Color::new(ppu.peek_ubyte(address));
				}
				i += 64;
			}

			let color = if sprite_data[0] < 240 { Color::new(0x2A) } else { Color::new(0x0F) };
			for tile_x in 0..8 {
				data[i + tile_x] = color;
			}
		}

		self.tx.send(data).unwrap();
		false
	}

}
