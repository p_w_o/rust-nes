use crate::nes::ppu::Color;


pub trait Graphics {
	fn draw(&mut self, picture: &[Color; 256 * 240]);

	fn set_fullscreen(&mut self, fullscreen: bool);

	fn set_fps(&mut self, fps: f64);

	fn set_show_fps(&mut self, show: bool);

	fn show_save_slot(&mut self, slot: u32, picture: Option<Vec<Color>>);

}
