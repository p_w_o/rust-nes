
pub enum Action {
	Quit,
	Save,
	Load,
	SaveSlotPrev,
	SaveSlotNext,
	Fullscreen,
	Windowed,
	FpsReset,
	FpsIncrease,
	FpsDecrease,
	FpsLimitToggle,
	FpsShowToggle,
}


pub struct InputState {
	pub actions: Vec<Action>,
	pub pads: [u8; 2],
}


pub trait Input {
	fn handle_input(&mut self) -> InputState;
}


pub enum NesButton {
	A       = 0b00000001,
	B       = 0b00000010,
	Select  = 0b00000100,
	Start   = 0b00001000,
	Up      = 0b00010000,
	Down    = 0b00100000,
	Left    = 0b01000000,
	Right   = 0b10000000,
}
